# Analysis workflow

Description of the analysis workflow, including instructions for visual QC of the analysis results.

## Experimental workflow

Pescoids with ubiquitous nuclear marker (GFP) were imaged on the MuVi SPIM system from Luxendo (Bruker).

Images were acquired at 16.6X magnification, resulting in a pixel size of 0.39 um (XY). To generate volumetric imaging, a Z plane was acquired every 2um.

Laser power was 20% (488 nm). 

Filter wheel used was BP 497-554. 

Beam expander: 3.5 um / 9.0 um.

Gaussian beam was synchronized with camera acquisition with slit size of 20 pixels (confocal line mode).

Exposure time was 50 ms with delays between acquisition of 11 ms.

Time interval between consecutive time points was 2 minutes 30 seconds.

## Preprocessing with Luxendo software

Raw data were pre-processed using the Luxendo image processor pipeline.

Images from the two camera were fused and downsampled to generate a volumetric image with isotropic resolution of 1 um (XYZ).

Fused images are saved with h5 format.

## Image analysis steps

### Dataset convertion

This step converts the images from the Luxendo file format (.h5) into the OME-NGFF file format (ome.zarr).

### Image registration

Pescoids move in the media during the experiment, therefore we need to register the dataset between consecutive timepoints to make sure the cell tracking works well.

To this aim, the following steps are taken:
- Use a blob detection algorithm to identify peak intensities (these will represent a fraction of the cells).
- Use iterative cloud point algorithm to register the clouds between two consecutive timpoints.
- Compute the registration transformation.
- Register and save dataset and MIPs (for visualization purposes).

### Image enhancement and segmentation

- On the original dataset, perform difference of gaussian to enhance blobs (cells) between a certain dimatere range, and save the anhanced dataset.
- Compute **foreground** pixels with a simple threshold binarization of the enhanced image.
- Compute **contour** probability with an inversion of the enhanced dataset.
- Register foreground and enhanced dataset using the transformations computed at previous step.
- Save the two dataset.

### Ultrack-ing

- Run ultrack algorithm with parameters store in the `config.toml` file and foreground and contours as input.
- Save segmentation dataset and tracks in the `results` folder.
