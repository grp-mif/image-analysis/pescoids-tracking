import os
import sys
import glob
import argparse
sys.path.append("funcs")
from _parse_args import parse_args
from _setup_folders import setup_folders
from _preprocess import preprocess
from _compute_foreground_and_contours import compute_foreground_and_contours_all_timepoints
from _run_ultrack import run_ultrack
from _compute_tracks_features import compute_tracks_features

'''
To run this script:

>>> python run_preprocessing.py --share /mif-users --radius_enhanced 1,1,1
'''

share, master_folder, dt, spot_diameter, threshold_spot_abs, radius_enhanced, threshold_foreground, sigma = parse_args()

################################################################################
# Use tp_max to test the tracking pipeline on a subset of timepoints (e.g. `tp_max = 5`).
# Use `tp_max = None` if you want to process the entire dataset.
################################################################################
tp_max = None

#--------------------------------------------------------

if __name__ == "__main__":

    paths = glob.glob(os.path.join(share, master_folder, "raw_data", "*"))
    paths.sort()
    # paths = paths[-2:]
    print(paths)

    for path in paths:
        print(10*"-",path)

        pescoid_id, processed_path, mips_path, results_path, tpmax_suffix = setup_folders(path, share, master_folder, tp_max)

        if not os.path.exists(f"{results_path}/my_segments.ome.zarr"):
            
            preprocess(path, processed_path, mips_path, 
                    dt, spot_diameter, threshold_spot_abs, radius_enhanced, 
                    tp_max = tp_max, 
                    tpmax_suffix = tpmax_suffix,
                    raw_zarrname = f"dataset{tpmax_suffix}.ome.zarr",
                    reg_zarrname = f"dataset_reg{tpmax_suffix}.ome.zarr", 
                    enh_zarrname = f"dataset_enh{tpmax_suffix}.ome.zarr", 
                    )

            compute_foreground_and_contours_all_timepoints(
                                processed_path,
                                threshold_foreground,
                                sigma,
                                tp_max = tp_max,
                                enh_zarrname = f"dataset_enh{tpmax_suffix}.ome.zarr",
                                foreground_zarrname = f"dataset_reg_foreground{tpmax_suffix}.ome.zarr",
                                contours_zarrname = f"dataset_reg_contours{tpmax_suffix}.ome.zarr",
                                )
            
            run_ultrack(
                processed_path,
                results_path,
                reg_zarrname = f"dataset_reg{tpmax_suffix}.ome.zarr", 
                foreground_zarrname = f"dataset_reg_foreground{tpmax_suffix}.ome.zarr",
                contours_zarrname = f"dataset_reg_contours{tpmax_suffix}.ome.zarr",
            )

        else:
            print("Dataset already processed.")
            
        if ( not os.path.exists(f"{results_path}/tracks_features.csv") )  or ( not os.path.exists(f"{results_path}/graph_dict.npy") ):
            
            compute_tracks_features(
                results_path,
            )
            
        else:
            print("Track features already computed.")
