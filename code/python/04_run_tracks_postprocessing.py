import os
import sys
import glob
import argparse
sys.path.append("funcs")
from _parse_args import parse_args
from _setup_folders import setup_folders
from _compute_tracks_features import compute_tracks_features

'''
To run this script:

>>> python run_preprocessing.py --share /mif-users --radius_enhanced 1,1,1
'''

share, master_folder, dt, spot_diameter, threshold_spot_abs, radius_enhanced, threshold_foreground, sigma = parse_args()


################################################################################
# Use tp_max is to test the tracking pipeline on a subset of timepoints.
# Use `tp_max = None` if you want to process the entire dataset.
################################################################################
tp_max = 5

#--------------------------------------------------------

if __name__ == "__main__":

    paths = glob.glob(os.path.join(share, master_folder, "raw_data", "*"))
    paths.sort()
    # paths = paths[-2:]
    print(paths)

    for path in paths:
        print(10*"-",path)

        pescoid_id, processed_path, mips_path, results_path, tpmax_suffix = setup_folders(path, share, master_folder, tp_max)
        print(results_path)
            
        # if not os.path.exists(f"{results_path}/tracks_features.csv"):
            
        compute_tracks_features(
            results_path,
        )
            
        # else:
        #     print("Track features already computed.")
