import glob, os, tqdm, time
import pandas as pd
from _read_meta_csv import read_meta_csv
from OpenIJTIFF import save_ij_tiff, open_ij_tiff
from _enhance_contrast import enhance_contrast, enhance_contrast_gpu

def enhance_contrast_folder(path,
                    radius_enhanced = 1., background=100.,
                    use_gpu=False
                ):
    meta = read_meta_csv(path)
    output_folder = path
    print(output_folder)
    ### Read all images and downsample

    print("Starting enhance contrast...")
    for tp in tqdm.tqdm(range(int(meta["n_tp"])), total=int(meta["n_tp"])):
        ### enhance contrast
        # print("Starting enhance contrast...")
        # start_enhance = time.time()
        image, ax_name, ax_scale, ax_unit = open_ij_tiff(os.path.join(output_folder, "ch-%d_tp-%03d_reg.tif"%(0,tp)))
        if not use_gpu:
            # print("Using CPU.")
            image_enhanced = enhance_contrast(image, ax_scale, radius=radius_enhanced)
            filename = "ch-%d_tp-%03d_enhanced.tif"%(0,tp)
        else:
            # print("Using GPU.")
            image_enhanced = enhance_contrast_gpu(image, ax_scale, radius=radius_enhanced, background=background)
            filename = "ch-%d_tp-%03d_enhanced_gpu.tif"%(0,tp)
        # print("--> Time to enhance contrast: %.2f minutes."%((time.time()-start_enhance)/60))
        _ = save_ij_tiff(
                        os.path.join(output_folder, filename),
                        image_enhanced,
                        ax_names = ax_name,
                        ax_scales = ax_scale,
                        ax_units = ax_unit,
                )
        
        
