import numpy as np
from skimage.filters import gaussian
import pyclesperanto_prototype as cle  # version 0.24.2
# import napari_pyclesperanto_assistant as ncle

def enhance_contrast(image, ax_scale, radius=1.):
    sigma = radius/ax_scale[0]
    # image_filt = gaussian(image.astype(float), sigma=sigma, truncate=3.0, preserve_range=True)
    image_filt = image.astype(float)
    g1 = gaussian(image_filt, sigma=[0.1, sigma, sigma], truncate=1.0, preserve_range=True)
    g2 = gaussian(image_filt, sigma=[0.1, 10.*sigma, 10.*sigma], truncate=1.0, preserve_range=True)
    g1 = np.clip(g1, 0, None)
    g2 = np.clip(g2, 0, None)
    image_enh = g1-g2
    image_enh = np.clip(image_enh, 0, None)
    image_enh = image_enh.astype(np.uint16)

    return image_enh

def enhance_contrast_gpu(image, ax_scale, radius=1., background=100.):
    sigma = radius/ax_scale[0]
    # image1 = cle.gaussian_blur(image, None, sigma/10., sigma/10., sigma/10.)
    # # image1 = image
    # # remove constant background (100) and set this to 0
    # image2 = cle.add_image_and_scalar(image1, None, -background)
    # image3 = cle.greater_constant(image2, None, 0.0)
    # image4 = cle.multiply_images(image3, image2)

    image4 = cle.top_hat_box(image, radius_x=sigma*3, radius_y=sigma*3, radius_z=0.)

    # perform difference of gaussian
    image5 = cle.difference_of_gaussian(image4, None, 
                                         sigma, sigma, 0., 
                                         10*sigma, 10*sigma, 0.)
    image6 = cle.greater_constant(image5, None, 0.0)
    image7 = cle.multiply_images(image6, image5)

    # perform intensity correction
    image_enh = cle.divide_by_gaussian_background(image7, None, 
                                         10*sigma, 10*sigma, 0.)
    # remove nans and set maximum values to remove unusually bright pixels
    image_enh = np.nan_to_num(image_enh,0)
    _max = np.percentile(image_enh, 99.9)
    image_enh = 1000*(image_enh/_max)
    image_enh = np.clip(image_enh, 0, 1000)
    image_enh = image_enh.astype(np.uint16)

    return image_enh

