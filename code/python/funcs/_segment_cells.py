import os, glob, time, tqdm, csv
import numpy as np
import pandas as pd
from OpenIJTIFF import save_ij_tiff, open_ij_tiff
from _watershed_segment import watershed_segment, watershed_segment_gpu
from _read_meta_csv import read_meta_csv

def segment_cells_folder(path, radius_gauss, min_distance, threshold_peak, 
                min_nucleus_radius, max_nucleus_radius, threshold_watershed,
                max_distance_to_10neighbors, use_gpu=False):
    meta = read_meta_csv(path)
        
    print("Starting cell segmentation...")
    for tp in tqdm.tqdm(range(int(meta["n_tp"])), total=int(meta["n_tp"])):
        # start = time.time()
        if not use_gpu:
            # print("Using CPU.")
            fname = os.path.join(path, "ch-%d_tp-%03d_enhanced.tif"%(0, tp))
            output_filename = "ch-0_tp-%03d_labels.tif"%tp
            watershed_function = watershed_segment
        else:
            # print("Using GPU.")
            fname = os.path.join(path, "ch-%d_tp-%03d_enhanced_gpu.tif"%(0, tp))
            output_filename = "ch-0_tp-%03d_labels_gpu.tif"%tp
            watershed_function = watershed_segment_gpu
    
        image_enhanced, ax_names, ax_scales, ax_units = open_ij_tiff(fname)

        image_labels = watershed_function(image_enhanced, ax_scales, 
                    radius_gauss, min_distance, threshold_peak, 
                    min_nucleus_radius, max_nucleus_radius, threshold_watershed, 
                    max_distance_to_10neighbors).astype(np.uint16)        
        
        _ = save_ij_tiff(
                os.path.join(path, output_filename),
                image_labels,
                ax_names = ax_names,
                ax_scales = ax_scales,
                ax_units = ax_units,
        )

        # print("--> Time to process: %.2f minutes."%((time.time()-start)/60))
