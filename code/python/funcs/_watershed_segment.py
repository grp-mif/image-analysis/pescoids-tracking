import time
import numpy as np
from _find_maxima import find_maxima
from _apply_watershed import apply_watershed
from scipy.ndimage import morphology
from skimage.filters import gaussian
from OpenIJTIFF import save_ij_tiff
import pyclesperanto_prototype as cle
from _filter_cells import filter_cells

def watershed_segment(image_enhanced, ax_scale, 
                      radius=1., min_distance=1, threshold_peak=20, 
                      min_nucleus_radius=2, max_nucleus_radius=25, threshold_watershed=50,
                      max_distance_to_10neighbors=50):
    
    ### apply mild smoothing and find local maxima in edt
    print("Starting maxima...")
    start_maxima = time.time()
    coords, markers = find_maxima(image_enhanced, ax_scale, radius, min_distance, threshold_peak=threshold_peak)
    print("Time to find maxima: %.2f minutes."%((time.time()-start_maxima)/60))

    print("Starting watershed...")
    start_watershed = time.time()
    labels = apply_watershed(image_enhanced, markers, ax_scale, min_nucleus_radius, max_nucleus_radius, threshold_watershed)
    print("Time to apply watershed: %.2f minutes."%((time.time()-start_watershed)/60))

    print("Starting filtering...")
    start_filtering = time.time()
    # labels = filter_cells(labels, ax_scale, max_distance_to_10neighbors)
    print("Time to filter: %.2f minutes."%((time.time()-start_filtering)/60))

    return labels.astype(np.uint16)

def watershed_segment_gpu(image, ax_scale, 
                      radius=1., min_distance=1, threshold_peak=70, 
                      min_nucleus_radius=2, max_nucleus_radius=25, threshold_watershed=50):
    ### apply mild smoothing and find local maxima in edt
    print("Starting maxima...")
    start_maxima = time.time()
    min_distance_pxl = int(min_distance//ax_scale[0])
    threshold_abs = np.percentile(image[image>0], threshold_peak)
    print("\t Min dist between peaks:",min_distance_pxl)
    print("\t Thresholds:", threshold_peak, threshold_abs)

    image3 = cle.greater_constant(image, None, threshold_abs)
    print("\t Binary done")
    image4 = cle.multiply_images(image, image3)
    print("\t Max of image:",np.max(image4))
    markers = cle.detect_maxima_box(image4, radius_x=min_distance_pxl, radius_y=min_distance_pxl, radius_z=min_distance_pxl)
    print("\t Number of peaks found:",np.sum(markers))
    print("Time to find maxima: %.2f minutes."%((time.time()-start_maxima)/60))

    print("Starting watershed...")
    start_watershed = time.time()
    print("\t Threshold watershed:",threshold_watershed)
    binary = cle.greater_constant(image, None, threshold_watershed)
    print("\t Binary done.")
    selected_spots = cle.binary_and(binary, markers)
    print("\t Filtered spots.")
    voronoi_diagram = cle.masked_voronoi_labeling(selected_spots, binary)
    print("\t Voronoi done.")

    min_volume_pxl = int((4*np.pi*min_nucleus_radius**3)/(3*ax_scale[0]*ax_scale[1]*ax_scale[2]))
    max_volume_pxl = int((4*np.pi*max_nucleus_radius**3)/(3*ax_scale[0]*ax_scale[1]*ax_scale[2]))  
    print("\t Filtered objects with size range:", min_volume_pxl, max_volume_pxl)
    labels = cle.exclude_labels_outside_size_range(voronoi_diagram, None, min_volume_pxl, max_volume_pxl).astype(np.uint16) 
    print("\t number of cells found:", np.max(labels))
    print("Time to apply watershed: %.2f minutes."%((time.time()-start_watershed)/60))
    
    return labels.astype(np.uint16)
