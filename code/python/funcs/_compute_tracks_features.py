import tqdm
import numpy as np
from ultrack.core.export.utils import solution_dataframe_from_sql
from ultrack.tracks.graph import add_track_ids_to_tracks_df, inv_tracks_df_forest
from ultrack import load_config

def compute_tracks_features(results_path):
    
    cfg_file = f"{results_path}/config.toml"
    cfg = load_config(cfg_file)
    cfg.data_config.working_dir = f"{results_path}"

    tracks_df = solution_dataframe_from_sql(cfg.data_config.database_path)
    tracks_df = add_track_ids_to_tracks_df(tracks_df)
    tracks_df.sort_values(by=["track_id", "t"], inplace=True)
    
    tracks_ids = list(set(tracks_df.track_id))
    tracks_ids.sort()
    
    ### compute velocity and distance
    instant_speeds = []
    avg_speeds = []
    cum_dists = []
    end_to_end_dists = []
    for track_id in tqdm.tqdm(tracks_ids, total=len(tracks_ids)):
        track_df = tracks_df[tracks_df.track_id==track_id]
        pos = track_df[["z","y","x"]].to_numpy()
        end_to_end_dist = np.sqrt(np.sum((pos[-1,:] - pos[0,:])**2))
        len_track = pos.shape[0]
        # print(pos.shape)

        if len_track>1:
            speed = list(np.sqrt(np.sum((pos[1:]-pos[:-1])**2,axis=1)))
            # print(len(speed))
            avg_speed = np.mean(speed)
            speed.append(0.)
            for v in speed:
                instant_speeds.append(v)
                avg_speeds.append(avg_speed)
                cum_dists.append(avg_speed*(len_track-1))
                end_to_end_dists.append( end_to_end_dist )
        else:
            instant_speeds.append(0)
            avg_speeds.append(0)
            cum_dists.append(0)
            end_to_end_dists.append(0)
    tracks_df["instant_speed"] = np.asarray(instant_speeds).astype(float)
    tracks_df["avg_speed"] = np.asarray(avg_speeds).astype(float)
    tracks_df["cum_dist"] = np.asarray(cum_dists).astype(float)
    tracks_df["end_to_end_dist"] = np.asarray(end_to_end_dists).astype(float)
    
    ### compute local cell density
    tracks_df.sort_values(by=["t"], inplace=True)
    tps = list(set(tracks_df.t))
    tps.sort()
    densities = []
    for tp in tqdm.tqdm(tps, total=len(tps)):
        cells_in_tp = tracks_df[tracks_df.t==tp]
        cells_in_tp_pos = cells_in_tp[["z","y","x"]].to_numpy()
        for i in range(cells_in_tp_pos.shape[0]):
            cell_pos = cells_in_tp_pos[i,:]
            dists = np.sqrt(np.sum((cells_in_tp_pos - cell_pos)**2, axis=1))
            density = np.sum(dists<25)
            densities.append(density)
    tracks_df["density"] = np.asarray(densities).astype(int)

    ### compute graph    
    tracks_df.sort_values(by=["track_id", "t"], inplace=True)
    graph = inv_tracks_df_forest(tracks_df)

    ### save to new csv file
    tracks_df.to_csv(f"{results_path}/tracks_features.csv")
    np.save(f"{results_path}/graph_dict.npy", graph) 
    
    return
    
    