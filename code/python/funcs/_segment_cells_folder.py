import os, tqdm
import numpy as np
from _read_meta_csv import read_meta_csv
from _ome_zarr import OmeZarr
from _enhance_contrast import enhance_contrast, enhance_contrast_gpu
from _watershed_segment import watershed_segment, watershed_segment_gpu

def segment_cells_folder(path,
                radius_gauss, min_distance, threshold_peak, 
                min_nucleus_radius, max_nucleus_radius, threshold_watershed,
                max_distance_to_10neighbors,
                tp_max = None,
                enh_zarrname = "dataset_enh.ome.zarr",
                labels_zarrname = "dataset_labels.ome.zarr",
                ):
    meta = read_meta_csv(path)
    if tp_max is not None:
        meta["n_tp"] = tp_max
    
    #################################################################################
    # Segment cells with watershed
    #################################################################################

    labels_zarr = os.path.join(path, labels_zarrname)
    
    if not os.path.exists(labels_zarr):
        
        enh_zarr = os.path.join(path, enh_zarrname)
        ds_enh = OmeZarr(enh_zarr, mode="r")

        _, _, ax_scale, ax_shape = ds_enh.get_axes_info( res_level=0 )
        dt = ax_scale[0]
        n_tp = ax_shape[0]

        label_ds = OmeZarr(labels_zarr, mode="a")
        label_ds.create_dataset(meta, dt = dt, n_scales = 1)

        for tp in tqdm.tqdm( range( n_tp ), total=n_tp ):
            
            channel = 0

            image_enhanced = ds_enh.read_stack( time_point=tp, channel=channel )

            image_labels = watershed_segment(image_enhanced, ax_scale[2:], 
                        radius_gauss, min_distance, threshold_peak, 
                        min_nucleus_radius, max_nucleus_radius, threshold_watershed, 
                        max_distance_to_10neighbors
                        ).astype(np.uint16)    

            label_ds.write_stack(image_labels, 
                                 timepoint=tp, 
                                 channel=channel
                                 )    

        label_ds.close()
        ds_enh.close()
        
    else:
        
        print("A labeled dataset already exists.")

