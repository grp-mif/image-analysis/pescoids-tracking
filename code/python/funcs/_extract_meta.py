import os, glob
import h5py

def extract_meta(path):
    print(path)
    files_path = glob.glob(os.path.join(path, "processed", "*_C"))[0]
    sample_meta = {}

    infos = path.split(os.sep)[-1]
    infos = infos.split("_")
    sample_meta["date"] = infos[0]

    n_h5 = (len(glob.glob(os.path.join(files_path,"*.h5")))-1)
    n_h5_ch0 = len(glob.glob(os.path.join(files_path,"*ch-0*.h5")))
    print(n_h5, n_h5_ch0)
    
    sample_meta["n_ch"] = n_h5//n_h5_ch0
    sample_meta["n_tp"] = n_h5//sample_meta["n_ch"]

    sample_meta["ch-%d_index"%0] = 0
    sample_meta["ch-%d_name"%0] = "nuclear"
    sample_meta["ch-%d_wavelength"%0] = 488

    meta_file = glob.glob(os.path.join(files_path, "*.xml"))[0]

    import xml.dom.minidom
    xml_doc = xml.dom.minidom.parse(meta_file)
    unit = xml_doc.getElementsByTagName("SpimData")[0].getElementsByTagName("ViewSetups")[0].getElementsByTagName("ViewSetup")[0].getElementsByTagName("voxelSize")[0].getElementsByTagName("unit")[0].childNodes[0].data
    size = xml_doc.getElementsByTagName("SpimData")[0].getElementsByTagName("ViewSetups")[0].getElementsByTagName("ViewSetup")[0].getElementsByTagName("voxelSize")[0].getElementsByTagName('size')[0].childNodes[0].data
    shape = xml_doc.getElementsByTagName("SpimData")[0].getElementsByTagName("ViewSetups")[0].getElementsByTagName("ViewSetup")[0].getElementsByTagName("size")[0].childNodes[0].data
    shape = [int(s) for s in shape.split(" ")]
    
    sample_meta["scale_unit"] = unit
    sample_meta["scale_z"] = [float(s) for s in size.split(" ")][2]
    sample_meta["scale_y"] = [float(s) for s in size.split(" ")][1]
    sample_meta["scale_x"] = [float(s) for s in size.split(" ")][0]
    sample_meta["shape_z"] = shape[2]
    sample_meta["shape_y"] = shape[1]
    sample_meta["shape_x"] = shape[0]

    return sample_meta

def test_extract_meta():
    path = os.path.join("//ebisuya.embl.es","ebisuya","input-tray","Cell_count",
                        "MC20230609_MM2_750_DAPI-405_SOX2-488_TBX6-594_BRA-647",
                        "2023-09-06_193223")
    sample_meta = extract_meta(path)
    print(sample_meta)
