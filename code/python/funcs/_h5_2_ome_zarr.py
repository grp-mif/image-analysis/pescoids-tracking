import glob, os, sys, math
import numpy as np
from tqdm import tqdm
import pandas as pd
from _extract_meta import extract_meta
from _open_and_downsample_h5 import open_h5
from ome_zarr.io import parse_url
import zarr
try:
    from cucim import __version__ as sk_version
    from cucim.skimage.transform import downscale_local_mean

    DOWNSCALE_METHOD = "cucim.skimage.transform.downscale_local_mean"

except ImportError:
    from skimage import __version__ as sk_version
    from skimage.transform import downscale_local_mean

    DOWNSCALE_METHOD = "skimage.transform.downscale_local_mean"

def h5_2_ome_zarr(inpath, outpath, zarrname="dataset.ome.zarr",
                  zyx_chunks=None, dt=2.5, n_scales=3, tp_max=-1):
    
    #################################################################################
    # extract metadata and save as csv file
    #################################################################################
    
    meta = extract_meta(inpath)
    if tp_max>0:
        meta["n_tp"] = tp_max
    meta = pd.Series(meta)
    print(meta)
    
    zarrpath = outpath+"/"+zarrname
    ZYXSHAPE = (int(meta["shape_z"]),int(meta["shape_y"]),int(meta["shape_x"]))
    if zyx_chunks is None:
        zyx_chunks = ZYXSHAPE

    n_tp = int(meta["n_tp"])
    n_ch = int(meta["n_ch"])

    store = parse_url(zarrpath, mode="w-").store
    root = zarr.group(store=store)

    arrays = []
    datasets = []

    for i in range(n_scales):
        factor = 2**i
        array_path = f"{i}"
        shape = (n_tp,n_ch,) + tuple(int(math.ceil(s / factor)) for s in ZYXSHAPE)
        chunks = (1,1,) + zyx_chunks
        ome_array = root.full(array_path, shape=shape, dtype=np.uint16, chunks=chunks, 
                                        compressor = zarr.Blosc(cname="zstd", clevel=3, shuffle=zarr.Blosc.BITSHUFFLE),
                                        fill_value = np.iinfo(np.uint16).max)
        arrays.append(ome_array)
        datasets.append(
            {
                "path": array_path,
                "coordinateTransformations": [
                    {
                        "type": "scale",
                        "scale": [dt,1.,float(meta["scale_z"])**i,float(meta["scale_y"])**i,float(meta["scale_x"]**i)],
                    }
                ],
            }
        )

    for ch in range(n_ch):
        for tp in tqdm(range(n_tp), "Converting channel %d: %s"%(ch,meta["ch-%d_name"%ch])):
            stack = open_h5(inpath, ch=ch, tp=tp)  # loading the data
            arrays[0][tp, ch] = stack
            for i in range(1, n_scales):
                factors = (2**i,) * stack.ndim
                arrays[i][tp, ch] = downscale_local_mean(stack, factors)
        
    root.attrs["multiscales"] = [
                {
                    "datasets": datasets,
                    "axes": [
                        {"name": "t", "type": "time", "unit": "minutes"},
                        {"name": "c", "type": "channel"},
                        {"name": "z", "type": "space", "unit": "micrometer"},
                        {"name": "y", "type": "space", "unit": "micrometer"},
                        {"name": "x", "type": "space", "unit": "micrometer"},
                    ],
                    "metadata": {
                        "method": DOWNSCALE_METHOD,
                        "version": sk_version,
                    },
                }
            ]

    def default_omero_metadata(name, channels, dtype):
        val_max = 1.0 if np.issubdtype(dtype, np.floating) else np.iinfo(dtype).max

        return {
            "id": 1,
            "name": name,
            "channels": [
                {
                    "active": True,
                    "coefficient": 1,
                    "color": "FFFFFF",
                    "family": "linear",
                    "inverted": False,
                    "label": str(ch),
                    "window": {"start": 0, "end": 1500, "min": 0, "max": val_max},  # guessing
                }
                for ch in channels
            ],
        }

    channels = range(n_ch)
    root.attrs["omero"] = default_omero_metadata(zarrpath, channels, np.uint16)
    
    return



# import glob, os, sys, math
# import numpy as np
# from tqdm import tqdm
# import pandas as pd
# from _extract_meta import extract_meta
# from _open_and_downsample_h5 import open_h5
# from ome_zarr.io import parse_url
# import zarr
# try:
#     from cucim import __version__ as sk_version
#     from cucim.skimage.transform import downscale_local_mean

#     DOWNSCALE_METHOD = "cucim.skimage.transform.downscale_local_mean"

# except ImportError:
#     from skimage import __version__ as sk_version
#     from skimage.transform import downscale_local_mean

#     DOWNSCALE_METHOD = "skimage.transform.downscale_local_mean"

# def h5_2_ome_zarr(inpath, outpath, zarrname="dataset.ome.zarr",
#                   zyx_chunks=(512,512,512), dt=2.5, n_scales=3, tp_max=-1):
    
#     #################################################################################
#     # extract metadata and save as csv file
#     #################################################################################
    
#     meta = extract_meta(inpath)
#     if tp_max>0:
#         meta["n_tp"] = tp_max
#     meta = pd.Series(meta)
#     print(meta)
    
#     zarrpath = outpath+"/"+zarrname
#     ZYXSHAPE = (int(meta["shape_z"]),int(meta["shape_y"]),int(meta["shape_x"]))

#     n_tp = int(meta["n_tp"])
#     n_ch = int(meta["n_ch"])

#     store = parse_url(zarrpath, mode="w-").store
#     root = zarr.group(store=store)

#     arrays = []
#     datasets = []

#     for i in range(n_scales):
#         factor = 2**i
#         array_path = f"{i}"
#         shape = (n_tp,n_ch,) + tuple(int(math.ceil(s / factor)) for s in ZYXSHAPE)
#         chunks = (1,1,) + zyx_chunks
#         ome_array = root.create_dataset(array_path, shape=shape, chunks=chunks, 
#                                         compressor=zarr.storage.default_compressor)
#         arrays.append(ome_array)
#         datasets.append(
#             {
#                 "path": array_path,
#                 "coordinateTransformations": [
#                     {
#                         "type": "scale",
#                         "scale": [dt,1.,float(meta["scale_z"])**i,float(meta["scale_y"])**i,float(meta["scale_x"]**i)],
#                     }
#                 ],
#             }
#         )

#     for ch in range(n_ch):
#         for tp in tqdm(range(n_tp), "Converting channel %d: %s"%(ch,meta["ch-%d_name"%ch])):
#             stack = open_h5(inpath, ch=ch, tp=tp)  # loading the data
#             arrays[0][tp, ch] = stack
#             for i in range(1, n_scales):
#                 factors = (2**i,) * stack.ndim
#                 arrays[i][tp, ch] = downscale_local_mean(stack, factors)
        
#     root.attrs["multiscales"] = [
#                 {
#                     "datasets": datasets,
#                     "axes": [
#                         {"name": "t", "type": "time", "unit": "minutes"},
#                         {"name": "c", "type": "channel"},
#                         {"name": "z", "type": "space", "unit": "micrometer"},
#                         {"name": "y", "type": "space", "unit": "micrometer"},
#                         {"name": "x", "type": "space", "unit": "micrometer"},
#                     ],
#                     "metadata": {
#                         "method": DOWNSCALE_METHOD,
#                         "version": sk_version,
#                     },
#                 }
#             ]

#     def default_omero_metadata(name, channels, dtype):
#         val_max = 1.0 if np.issubdtype(dtype, np.floating) else np.iinfo(dtype).max

#         return {
#             "id": 1,
#             "name": name,
#             "channels": [
#                 {
#                     "active": True,
#                     "coefficient": 1,
#                     "color": "FFFFFF",
#                     "family": "linear",
#                     "inverted": False,
#                     "label": ch,
#                     "window": {"start": 0, "end": 1500, "min": 0, "max": val_max},  # guessing
#                 }
#                 for ch in channels
#             ],
#         }

#     channels = ["nuclear"]
#     root.attrs["omero"] = default_omero_metadata(zarrpath, channels, np.uint16)
    
#     return

