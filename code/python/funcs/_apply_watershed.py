import time
import numpy as np
from skimage.segmentation import watershed
from skimage.morphology import remove_small_objects
from skimage.measure import label

def apply_watershed(image, markers, ax_scale, min_nucleus_radius=1, max_nucleus_radius=1e6, threshold_watershed=50):
    print("\tThreshold watershed:",threshold_watershed)
    image_mask = image>threshold_watershed
    labels = watershed(-(image.astype(float)), 
                                    markers, 
                                    mask=image_mask
                                    )
    min_volume_pxl = int((4*np.pi*min_nucleus_radius**3)/(3*ax_scale[0]*ax_scale[1]*ax_scale[2]))
    max_volume_pxl = int((4*np.pi*max_nucleus_radius**3)/(3*ax_scale[0]*ax_scale[1]*ax_scale[2]))
    
    print("\tCell volume lims (pxls):",min_volume_pxl, max_volume_pxl)

    # remove small and large objects
    small_removed = remove_small_objects(labels, min_size=min_volume_pxl)
    mid_removed = remove_small_objects(labels, min_size=max_volume_pxl)
    image_filt = small_removed - mid_removed

    image_filt = label(image_filt)
    print("\tNumber of cells found:", np.max(image_filt))
    
    return image_filt.astype(np.uint16)
