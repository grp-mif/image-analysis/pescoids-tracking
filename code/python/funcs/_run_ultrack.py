import os, tqdm, toml
import numpy as np
import dask.array as da
from rich.pretty import pprint
from _ome_zarr import OmeZarr
from typing import Any, List, Optional, Sequence, Tuple, Union

from ultrack import MainConfig, load_config, track, to_tracks_layer, tracks_to_zarr

def run_ultrack(
        processed_path: str,
        ultrack_path: str,
        reg_zarrname: Optional[str] = "dataset_reg.ome.zarr", 
        foreground_zarrname: Optional[str] = "dataset_reg_foreground.ome.zarr",
        contours_zarrname: Optional[str] = "dataset_reg_contours.ome.zarr",
):
    """_summary_

    Args:
        processed_path (str): _description_
        reg_zarrname (Optional[str], optional): _description_. Defaults to "dataset_reg.ome.zarr".
        foreground_zarrname (Optional[str], optional): _description_. Defaults to "dataset_reg_foreground.ome.zarr".
        contours_zarrname (Optional[str], optional): _description_. Defaults to "dataset_reg_contours.ome.zarr".
        ultrack_foldername (Optional[str], optional): _description_. Defaults to "results_ultrack".
    """
    
    dataset_zarr = OmeZarr(f"{processed_path}/{reg_zarrname}", mode="r")
    ax_info = dataset_zarr.get_axes_info( res_level=0 )
    voxel_size = ax_info[2][2:]

    foreground_zarr = OmeZarr(f"{processed_path}/{foreground_zarrname}", mode="r")
    foreground = foreground_zarr.root[0]

    contours_zarr = OmeZarr(f"{processed_path}/{contours_zarrname}", mode="r")
    contours = contours_zarr.root[0]

    # cfg = MainConfig()
    print("\tLoading config file")
    cfg_file = f"{ultrack_path}/config.toml"
    cfg = load_config(cfg_file)
    cfg.data_config.working_dir = f"{ultrack_path}"
    pprint(cfg)

    print("\tTracking")
    track(
        cfg,
        detection=da.from_zarr(foreground)[:,0,...],
        edges=da.from_zarr(contours)[:,0,...],
        scale=voxel_size,
        overwrite=True,
    )

    print("\tSaving tracks.csv")
    tracks_df, graph = to_tracks_layer(cfg)
    # tracks_df.t = tracks_df.t*2.5
    tracks_df.to_csv(f"{ultrack_path}/tracks.csv", index=False)

    print("\tSaving to ome.zarr.")
    segments = tracks_to_zarr(
        cfg,
        tracks_df,
        store_or_path=f"{ultrack_path}/segments.zarr",
        overwrite=True,
    )

    meta = {
        "n_tp":segments.shape[0],
        "n_ch":1,
        "shape_z":segments.shape[1],
        "shape_y":segments.shape[2],
        "shape_x":segments.shape[3],
        "scale_z":1.,
        "scale_y":1.,
        "scale_x":1.,
    }
    dt=1
    n_scales=1
    zyx_chunks=(64,256,256)

    s = OmeZarr(f"{ultrack_path}/my_segments.ome.zarr")
    s.create_dataset(meta, dt = dt, 
                     n_scales = n_scales, 
                     zyx_chunks = zyx_chunks, 
                     dtype = np.int32
                     )

    for tp in tqdm.tqdm(range(meta["n_tp"]), total=meta["n_tp"]):
        ch = 0
        s.write_stack(segments[tp], timepoint = tp, channel = ch)

    return

