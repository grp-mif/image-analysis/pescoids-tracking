import os
import shutil

def setup_folders(path, share, master_folder, tp_max):
    infos = path.split(os.sep)[-1]
    datetime = infos.split("_")[0]
    pescoid_id = f"pescoid_{datetime}"

    if tp_max is None:
        tpmax_suffix = ""
    else:
        tpmax_suffix = f"_{tp_max}tps"

    processed_path = f"{share}/{master_folder}/processed_data/{pescoid_id}"
    mips_path = f"{processed_path}/mips"
    if not os.path.exists(processed_path):
        os.makedirs(processed_path)
    if not os.path.exists(mips_path):
        os.makedirs(mips_path)
    
    results_path = f"{share}/{master_folder}/results/{pescoid_id}"
    ultrack_foldername = f"results_ultrack{tpmax_suffix}"
    ultrack_path = f"{results_path}/{ultrack_foldername}"
    if not os.path.exists(results_path):
        os.makedirs(results_path)
    if not os.path.exists(ultrack_path):
        os.makedirs(ultrack_path)
        
        shutil.copy(f"../../documentation/config.toml",
                    f"{ultrack_path}/config.toml")
    
    return pescoid_id, processed_path, mips_path, ultrack_path, tpmax_suffix

