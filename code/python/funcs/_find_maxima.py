
import numpy as np
from skimage.feature import peak_local_max
from skimage.filters import gaussian
from skimage.measure import label

def find_maxima(image, ax_scale, radius=1., min_distance=1, threshold_peak=20):
    ### apply mild smoothing and find local maxima
    sigma = radius/ax_scale[0]
    min_distance_pxl = np.max([int(min_distance//ax_scale[0]), 1])
    image_gauss = gaussian(image, sigma=[sigma, sigma, sigma], truncate=1.0, preserve_range=True).astype(np.uint16)
    # image_gauss = image
    print("\tFind maxima...")
    print("\tThreshold peak:",threshold_peak)
    coords = peak_local_max(image_gauss, 
                            min_distance=min_distance_pxl, 
                            threshold_abs=threshold_peak
                        )
    print("\tNumber of peaks found:", coords.shape[0])
    print("\tGenerate seed image...")
    markers = np.zeros(image.shape, dtype=bool)
    markers[tuple(coords.T)] = True
    markers = label(markers)

    return coords, markers

