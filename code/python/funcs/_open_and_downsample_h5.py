import os, glob
import h5py
import numpy as np
from skimage.transform import rescale
import os, glob
import h5py
import numpy as np
from skimage.transform import rescale

# def open_downsample_h5(path, ch, sample_meta, desired_scale=1):
def open_h5(path, ch, tp):
    files_path = glob.glob(os.path.join(path, "processed", "*_C"))[0]
    # print(path)

    # print(proc_folder)
    file_name = glob.glob(os.path.join(files_path, "*tp-%d_ch-%d*.lux.h5"%(tp,ch)))[0]
    # print("\tLoading from this file:",file_name.split(os.sep)[-1])


    with h5py.File(file_name, "r") as f:
        # print(f.keys())
        # print(dims)

        # print("\tCh: %d -> Extracting \"Data\" dataset with scale (ZYX): scale 1."%(ch))
        # print("\t", scales)
        # print("\t", desired_scale)
        dataset = f["Data"][()]
        dataset = dataset.astype(np.uint16)
        
    return dataset

def test_open_and_downsample_h5():
    path = os.path.join("/mif-users","Users","Laura_Bianchi",
                        "2023-11-20_170158","processed","20231204-072053_Task_1_sample0_control_C")
    meta = extract_meta(path)

    dataset = read_channel(path, ch=1, tp=12, sample_meta=meta, downXY=1)
