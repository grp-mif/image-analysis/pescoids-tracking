import glob, os, sys, math
from typing import Any, List, Optional, Sequence, Tuple, Union
import numpy as np
from tqdm import tqdm
import pandas as pd
from ome_zarr.io import parse_url
import zarr
import dask.array as da
try:
    from cucim import __version__ as sk_version
    from cucim.skimage.transform import downscale_local_mean

    DOWNSCALE_METHOD = "cucim.skimage.transform.downscale_local_mean"

except ImportError:
    from skimage import __version__ as sk_version
    from skimage.transform import downscale_local_mean

    DOWNSCALE_METHOD = "skimage.transform.downscale_local_mean"

class OmeZarr():
    
    def __init__(self, 
                 zarrpath: str,
                 mode: str = "a"
                 ):
        """Instantiate a zarr dataset.

        Args:
            zarrpath (str): Path to ome.zarr file
            mode (str, optional): Access mode. Defaults to "a" (read/write).
        """
        
        self.zarrpath = zarrpath
        self.store = parse_url(zarrpath, mode=mode).store
        self.root = zarr.group(store=self.store)
        
    def create_dataset(self,
                meta: dict,
                dt: float = 2.5,
                n_scales: int = 1,
                zyx_chunks: Optional[Sequence[int]] = None,
                channel_colors: Optional[Sequence[str]] = None,
                dtype = np.uint16,
                ):
        """Generate a group with datasets and metadata. Requires mode="a" or "w".

        Args:
            meta (dict): Dictionary with relevant metadata.
            dt (float, optional): Known time resolution if timelapse (in minutes). Defaults to 2.5.
            n_scales (int, optional): Number of resolution layers. Defaults to 1.
            zyx_chunks (Optional[Sequence[int]], optional): 3D chunk size (Z-Y-X). Defaults to None.
            channel_colors (Optional[Sequence[str]], optional): Display colors of channels (as hexa string). Defaults to None.

        """

        zyx_shape = (int(meta["shape_z"]),int(meta["shape_y"]),int(meta["shape_x"]))
        if zyx_chunks is None:
            zyx_chunks = zyx_shape
            
        datasets = []
        try:
            val_max = np.iinfo(dtype).max
        except:
            val_max = np.finfo(dtype).max

        for i in range(n_scales):
            factor = 2**i
            array_path = f"{i}"
            shape = (meta["n_tp"],meta["n_ch"],) + tuple(int(math.ceil(s / factor)) for s in zyx_shape)
            chunks = (1,1,) + tuple([math.ceil(c/factor) for c in zyx_chunks])
            self.root.full(array_path, shape=shape, dtype=dtype, chunks=chunks, 
                                            compressor = zarr.Blosc(cname="zstd", clevel=5, shuffle=zarr.Blosc.BITSHUFFLE),
                                            fill_value = val_max)
            datasets.append(
                {
                    "path": array_path,
                    "coordinateTransformations": [
                        {
                            "type": "scale",
                            "scale": [dt,1.,float(meta["scale_z"])*factor,float(meta["scale_y"])*factor,float(meta["scale_x"])*factor],
                        }
                    ],
                }
            )

        self.root.attrs["multiscales"] = [
                {
                    "datasets": datasets,
                    "axes": [
                        {"name": "t", "type": "time", "unit": "minutes"},
                        {"name": "c", "type": "channel"},
                        {"name": "z", "type": "space", "unit": "micrometer"},
                        {"name": "y", "type": "space", "unit": "micrometer"},
                        {"name": "x", "type": "space", "unit": "micrometer"},
                    ],
                    "metadata": {
                        "method": DOWNSCALE_METHOD,
                        "version": sk_version,
                    },
                }
            ] 
        
        def default_omero_metadata(name, channels, channel_colors=None):
            try:
                val_max = np.iinfo(dtype).max
            except:
                val_max = np.finfo(dtype).max
            
            if channel_colors is None:
                colors = ["FFFFFF", "00FFFF", "00FF00", "FF00FF", "FF0000"]*10
                channel_colors = [colors[ch] for ch in channels]

            return {
                "id": 1,
                "name": name,
                "channels": [
                    {
                        "active": True,
                        "coefficient": 1,
                        "color": channel_colors[ch],
                        "family": "linear",
                        "inverted": False,
                        "label": str(ch),
                        "window": {"start": 0, "end": 1500, "min": 0, "max": val_max},  # guessing
                    }
                    for ch in channels
                ],
            }          
        
        self.root.attrs["omero"] = default_omero_metadata(self.zarrpath, 
                                                          range(int(meta["n_ch"])), 
                                                          channel_colors=channel_colors,
                                                          )

    def get_n_scales(self):
        
        return len(sorted(self.root.array_keys()))
    
    def get_axes_info(self,
                  res_level: Optional[int]=0):
        
        meta = self.root.attrs.asdict()
        ax_name = tuple( [ a["name"] for a in meta["multiscales"][0]["axes"] ] )
        ax_unit = tuple( [ a["unit"] if "unit" in a.keys() else "" for a in meta["multiscales"][0]["axes"] ] )
        ax_scale = meta["multiscales"][0]["datasets"][res_level]["coordinateTransformations"][0]["scale"]
        ax_shape = self.root[res_level].shape
        
        return ax_name, ax_unit, ax_scale, ax_shape

    def write_stack(self,
                    stack,
                    timepoint: int,
                    channel: int):
        """Write a stack into the dataset.

        Args:
            stack (np.ndarray or da.Array): Image stack (3D, ZYX).
            timepoint (int): Time index.
            channel (int): Channel index.
        """
        
        ###
        # https://forum.image.sc/t/writing-tile-wise-ome-zarr-with-pyramid-size/85063/2
        # https://zarr.readthedocs.io/en/stable/api/hierarchy.html#zarr.hierarchy.Group.require_dataset
        ###
        n_scales = self.get_n_scales()

        zyx_shapes = [ self.root[i].shape[2:] for i in range(n_scales)]
        zyx_chunk_sizes = [ self.root[i].chunks[2:] for i in range(n_scales)]

        zarrays = [ self.root.require_dataset(
            str(i),
            shape = self.root[i].shape,
            exact = True,
            chunks = self.root[i].chunks,
            dtype = self.root[i].dtype,
        ) for i in range(n_scales) ]
        
        # print([type(zarray), zarray.shape) for zarray in zarrays])
        
        tile_idx = [math.ceil(s/c) for s,c in zip( zyx_shapes[0], zyx_chunk_sizes[0] )]
        
        for z_idx in range(tile_idx[0]):
            
            for y_idx in range(tile_idx[1]):
                
                for x_idx in range(tile_idx[2]):
                    
                    # print("z:%d/%d - y:%d/%d - x:%d/%d"%(z_idx,tile_idx[0],y_idx,tile_idx[1],x_idx,tile_idx[2]))
                    z1 = z_idx * zyx_chunk_sizes[0][0]
                    z2 = z1 + zyx_chunk_sizes[0][0]
                    y1 = y_idx * zyx_chunk_sizes[0][1]
                    y2 = y1 + zyx_chunk_sizes[0][1]
                    x1 = x_idx * zyx_chunk_sizes[0][2]
                    x2 = x1 + zyx_chunk_sizes[0][2]
                    
                    if isinstance(stack, np.ndarray):
                        tile = stack[z1:z2, y1:y2, x1:x2]
                    elif isinstance(stack, da.Array):
                        tile = stack[z1:z2, y1:y2, x1:x2].compute()
                        
                    zarrays[0][timepoint, channel, z1:z2, y1:y2, x1:x2] = tile
            
                    for i in range(1, n_scales):
                        z1 = z_idx * zyx_chunk_sizes[i][0]
                        z2 = z1 + zyx_chunk_sizes[i][0]
                        y1 = y_idx * zyx_chunk_sizes[i][1]
                        y2 = y1 + zyx_chunk_sizes[i][1]
                        x1 = x_idx * zyx_chunk_sizes[i][2]
                        x2 = x1 + zyx_chunk_sizes[i][2]

                        # print("Resolution level:", i)
                        factors = (2**i,) * stack.ndim
                        tile_down = downscale_local_mean(tile, factors)
                        # print(zarrays[1][timepoint, channel, z1:z2, y1:y2, x1:x2].shape)
                        # print(tile_down.shape)
                        zarrays[i][timepoint, channel, z1:z2, y1:y2, x1:x2] = tile_down
                    # print(type(stack), stack.shape)
                    # print(type(zarray), zarray.shape)
                    # print(type(tile), tile.shape)
                    # print(type(zarray[timepoint, channel, z1:z2, y1:y2, x1:x2]), zarray[timepoint, channel, z1:z2, y1:y2, x1:x2].shape)
            
    def read_stack(self, 
                   time_point: int, 
                   channel: int, 
                   res_level: Optional[int] = 0):
        """Read a single timepoint/channel as a numpy ndarray.

        Args:
            time_point (int): Index of the timepoint to open.
            channel (int): Index of the channel to open.
            res_level (Optional[int], optional): Resolution layer to open. Defaults to 0.

        Returns:
            ndarray: Stack as numpy array.
        """
        
        zarray = self.root.require_dataset(
                    str(res_level),
                    shape = self.root[res_level].shape,
                    exact = True,
                    chunks = self.root[res_level].chunks,
                    dtype = self.root[res_level].dtype,
                )
        
        ndarray = zarray[time_point, channel, ...]
           
        return ndarray
                
    def close(self):
        
        self.store.close()       
                