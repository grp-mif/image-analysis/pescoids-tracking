import os, tqdm
import numpy as np
from _read_meta_csv import read_meta_csv
from OpenIJTIFF import save_ij_tiff, open_ij_tiff
from _enhance_contrast import enhance_contrast, enhance_contrast_gpu
from _watershed_segment import watershed_segment, watershed_segment_gpu

def enhance_contrast_and_segment_folder(path,
                    radius_enhanced, background,
                    radius_gauss, min_distance, threshold_peak, 
                    min_nucleus_radius, max_nucleus_radius, threshold_watershed,
                    max_distance_to_10neighbors, use_gpu=False, save_enhanced = False,
                    ):
    meta = read_meta_csv(path)
    # print(path)

    for tp in tqdm.tqdm( range( int(meta["n_tp"]) ), total=int(meta["n_tp"]) ):

        image, ax_name, ax_scale, ax_unit = open_ij_tiff(os.path.join(path, "ch-%d_tp-%03d_reg.tif"%(0,tp)))

        if not use_gpu:
            image_enhanced = enhance_contrast(image, ax_scale, radius=radius_enhanced)
            watershed_function = watershed_segment
            enh_filename = "ch-%d_tp-%03d_enhanced.tif"%(0,tp)
            seg_filename = "ch-%d_tp-%03d_labels.tif"%(0,tp)

        else:
            image_enhanced = enhance_contrast_gpu(image, ax_scale, radius=radius_enhanced, background=background)
            watershed_function = watershed_segment_gpu
            enh_filename = "ch-%d_tp-%03d_enhanced_gpu.tif"%(0,tp)
            seg_filename = "ch-%d_tp-%03d_labels_gpu.tif"%(0,tp)
            
        if save_enhanced:
            _ = save_ij_tiff(
                        os.path.join(path, enh_filename),
                        image_enhanced,
                        ax_names = ax_name,
                        ax_scales = ax_scale,
                        ax_units = ax_unit,
                )

        image_labels = watershed_function(image_enhanced, ax_scale, 
                    radius_gauss, min_distance, threshold_peak, 
                    min_nucleus_radius, max_nucleus_radius, threshold_watershed, 
                    max_distance_to_10neighbors).astype(np.uint16)        
        
        _ = save_ij_tiff(
                os.path.join(path, seg_filename),
                image_labels,
                ax_names = ax_name,
                ax_scales = ax_scale,
                ax_units = ax_unit,
        )
