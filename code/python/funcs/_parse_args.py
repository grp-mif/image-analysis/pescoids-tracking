import argparse

def list_of_floats(arg):
    l = [float(f) for f in arg.split(",")]
    # print("---",l)
    return l

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--share", 
                        type=str, default="/g/trivedi", 
                        help="Server share. /g/trivedi (Linux) or //trivedi.embl.es/trived (Windows)")
    parser.add_argument("--master_folder",
                        type=str, default="Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking")

    parser.add_argument("--dt",
                        type=float, default=1.)

    ### Parameters for image registration
    parser.add_argument("--spot_diameter",
                        type=int, default=10,
                        help="Diameter of the spots for registration.")
    parser.add_argument("--threshold_spot_abs",
                        type=int, default=50,
                        help="Spots with lower intensity will be ignored.")

    ### Parameters for cell segmentation
    parser.add_argument("--radius_enhanced",
                        type=list_of_floats, default=1.,
                        help="Desired radius enhancement in um (provide one value for isotropic ZYX dimension).")
    parser.add_argument("--threshold_foreground", 
                        type=int, default=15,
                        help="Watershed threshold (in percentile).")
    parser.add_argument("--sigma", 
                        type=int, default=1,
                        help="Sigma for mild blur to find contours.")

    args = parser.parse_args()

    share = args.share
    master_folder = args.master_folder

    dt = args.dt

    spot_diameter = args.spot_diameter
    threshold_spot_abs = args.threshold_spot_abs

    radius_enhanced = args.radius_enhanced

    threshold_foreground = args.threshold_foreground
    sigma = args.sigma

    return share, master_folder,dt, spot_diameter, threshold_spot_abs, radius_enhanced, threshold_foreground, sigma