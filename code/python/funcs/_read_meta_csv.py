import os, csv

def read_meta_csv(path):
    
    reader = csv.reader(open(os.path.join(path, "meta.csv")))
    meta = {}
    for row in reader:
        meta[row[0]] = row[1]
    # print(meta)
    
    if "/" in meta["date"]:
        splits = meta["date"].split("/")
        meta["date"] = splits[2]+"-"+splits[0]+"-"+splits[1]

    return meta
