import glob, os, tqdm, time
import numpy as np
import pandas as pd
from _extract_meta import extract_meta
from _read_lux_h5_as_dask import read_lux_h5_as_dask
from _optireg import find_anchor_points, find_transformations, perform_image_registration
from OpenIJTIFF import save_ij_tiff
from _enhance_contrast import enhance_contrast
from _ome_zarr import OmeZarr
from typing import Any, List, Optional, Sequence, Tuple, Union

def preprocess(raw_path: str,
               processed_path: str,
               mips_path: str,
               dt: Optional[float] = .1, 
               cell_diameter: Optional[float] = 10, 
               threshold: Optional[float] =50, 
               radius_enhanced: Optional[float] = 2.,
               channel_colors: Optional[Sequence[str]] = None,
               n_scales: Optional[int] = 2,
               tp_max: Optional[int] = None,
               tpmax_suffix: Optional[str] = "",
               raw_zarrname: Optional[str] = "dataset.ome.zarr",
               transformation_filename: Optional[str] = "transformations.npz",
               reg_zarrname: Optional[str] = "dataset_reg.ome.zarr", 
               enh_zarrname: Optional[str] = "dataset_enh.ome.zarr", 
               ):
    """_summary_

    Args:
        raw_path (str): _description_
        processed_path (str): _description_
        mips_path (str): _description_
        dt (Optional[float], optional): _description_. Defaults to .1.
        cell_diameter (Optional[float], optional): _description_. Defaults to 10.
        threshold (Optional[float], optional): _description_. Defaults to 50.
        radius_enhanced (Optional[float], optional): _description_. Defaults to 2..
        channel_colors (Optional[Sequence[str]], optional): _description_. Defaults to None.
        n_scales (Optional[int], optional): _description_. Defaults to 2.
        tp_max (Optional[int], optional): _description_. Defaults to None.
        tpmax_suffix (Optional[str], optional): _description_. Defaults to "".
        raw_zarrname (Optional[str], optional): _description_. Defaults to "dataset.ome.zarr".
        transformation_filename (Optional[str], optional): _description_. Defaults to "transformations.npz".
        reg_zarrname (Optional[str], optional): _description_. Defaults to "dataset_reg.ome.zarr".
        enh_zarrname (Optional[str], optional): _description_. Defaults to "dataset_enh.ome.zarr".
    """

    #################################################################################
    # extract metadata and save as csv file
    #################################################################################
    
    meta = extract_meta(raw_path)
    if tp_max is not None:
        meta["n_tp"] = tp_max
    print(meta)

    meta = pd.Series(meta)
    meta.to_csv(f"{processed_path}/meta.csv")
    
    #################################################################################
    # Resave dataset as ome zarr
    #################################################################################
    
    raw_zarr = f"{processed_path}/{raw_zarrname}"
    
    print("---> 01")
    
    if not os.path.exists(raw_zarr):
        
        print("Converting dataset to ome-zarr format.")

        ds_raw = OmeZarr(raw_zarr, mode = "a")
        ds_raw.create_dataset(meta, dt = dt, n_scales = n_scales, channel_colors = channel_colors)

        files_path = glob.glob(os.path.join(raw_path, "processed", "*_C"))[0]
        file_name = glob.glob(os.path.join(files_path, "*tp-0_ch-0*.lux.h5"))[0]
        file_name = file_name.replace("tp-0","tp-%d") 
        file_name = file_name.replace("ch-0","ch-%d") 
    
        image, _, _ = read_lux_h5_as_dask(meta, file_name, zyx_chunks=None) # image is da.Array

        for ch in range(meta["n_ch"]):
        
            for tp in tqdm.tqdm(range(meta["n_tp"]), total=meta["n_tp"]):
                
                ds_raw.write_stack(image[tp, ch], tp, ch )

        ds_raw.close()
        
    else:
        
        print("Dataset already converted to ome.zarr.")
               

    #################################################################################
    # Compute image transformations to register timepoints
    #################################################################################

    print("---> 02")
    
    transformation_name = f"{processed_path}/{transformation_filename}"
    
    if not os.path.exists( transformation_name ):
        
        print("Compute transformation...")
        
        ds_raw = OmeZarr(raw_zarr, mode="r")
        _, _, _, ax_shape = ds_raw.get_axes_info()
        n_tp = ax_shape[0]
    
        ### Read images and identify majority of cells
        
        print("Finding anchor points.")

        df = pd.DataFrame({})
        
        for tp in tqdm.tqdm(range(n_tp), total=n_tp):
            
            ch = 0
            
            image = ds_raw.read_stack(channel=ch, time_point=tp)
        
            df1 = find_anchor_points(image, tp, cell_diameter_XY = cell_diameter, threshold = threshold)
        
            df = pd.concat([df,df1], ignore_index=True)

        # reverse_order
        df.loc[:,'tpreg'] = max(df.tp)-df.tp
        
        ### Compute transformation and save them
        
        print("Compute and save transformations.")
        
        tps = list(set(df.tpreg))
        tps.sort()
        
        df_list = [df[df.tpreg==tp] for tp in tps]

        Ts = find_transformations(df_list, transformation_name, maxiter=100, tol=1e-5)

        ### df_list = perform_points_registration(df_list, Ts)
        
        ds_raw.close()
        
    else:
        
        ### If it was already computed, load transformation matrices
        
        print("Found transformation. Loading...")
        Ts = np.load( transformation_name )["Ts"]
        
    #################################################################################
    # Apply transformation to images and save
    #################################################################################

    print("---> 03")

    reg_zarr = f"{processed_path}/{reg_zarrname}"
    enh_zarr = f"{processed_path}/{enh_zarrname}"
    
    if ( not os.path.exists(reg_zarr) ) or ( not os.path.exists(enh_zarr) ):
        
        print("Enhance and register images.")
        
        ds_raw = OmeZarr(raw_zarr, mode="r")
        _, _, ax_scale, ax_shape = ds_raw.get_axes_info()
        n_tp = ax_shape[0]
        n_ch = ax_shape[1]
        
        ds_reg = OmeZarr(reg_zarr, mode = "a")
        ds_reg.create_dataset(meta, dt = dt, n_scales = n_scales, channel_colors = channel_colors)

        ds_enh = OmeZarr(enh_zarr, mode = "a")
        ds_enh.create_dataset(meta, dt = dt, n_scales = n_scales, channel_colors = channel_colors)

        for ch in range(n_ch):
        
            for tp in tqdm.tqdm(range(n_tp), total=n_tp):
                
                raw_image = ds_raw.read_stack( channel=ch, time_point=tp )
            
                image_reg = perform_image_registration( raw_image, tp, Ts ) # NOTE: image_reg is np.ndarray

                image_enh = enhance_contrast( raw_image, ax_scale[2:], radius=radius_enhanced )
                
                # image_enh = perform_image_registration( image_enh, tp, Ts )
                
                ds_reg.write_stack(image_reg, tp, ch )
                ds_enh.write_stack(image_enh, tp, ch)

        ds_raw.close()
        ds_reg.close()
        ds_enh.close()
        
    else:
        
        print("A registered and enhanced dataset already exists.")
                
    #################################################################################
    # Compute MIPs and save
    #################################################################################

    print("---> 04")
    
    ds_reg = OmeZarr(reg_zarr, mode="r")
    _, _, ax_scale, ax_shape = ds_reg.get_axes_info()
    n_tp = ax_shape[0]
    n_ch = ax_shape[1]
            
    for ch in range(n_ch):
            
        if not f"{mips_path}/ch-{ch}_mip{tpmax_suffix}.tif":
            
            print("Computing MIP for channel %d."%(ch))
            
            mip = [[] for i in range(meta["n_tp"])]
            
            for tp in tqdm.tqdm(range(meta["n_tp"]), total=meta["n_tp"]):
        
                stack = ds_reg.read_stack(channel=ch, time_point=tp)
                
                # image, _, _, _ = open_ij_tiff(os.path.join(output_folder, "ch-%d_tp-%03d_reg.tif"%(ch,tp)))
            
                image_mip = np.max(stack, axis=0)
            
                mip[tp] = image_mip

            _ = save_ij_tiff(
                        f"{mips_path}/ch-{ch}_mip{tpmax_suffix}.tif",
                        np.array(mip),
                        ax_names = "TYX",
                        ax_scales = [1. for i in "TYX"],
                        ax_units = [1, "um", "um"],
                )
            
        else:
            
            print("MIP of channel %d already computed."%ch)
      
    ds_reg.close()
