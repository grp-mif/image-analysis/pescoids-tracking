import os, tqdm
import numpy as np
from skimage.filters import gaussian
from _optireg import perform_image_registration
from _read_meta_csv import read_meta_csv
from _ome_zarr import OmeZarr
from typing import Any, List, Optional, Sequence, Tuple, Union

def compute_foreground_and_contours(image,
                                    threshold_foreground,
                                    sigma,
                                    lower_perc=0.01,
                                    upper_perc=99.99,
                                ):
    
    foreground = image>threshold_foreground
    
    image_gauss = gaussian(image, sigma=[sigma, sigma, sigma], truncate=1.0, preserve_range=True)
    
    im_min = np.percentile(image_gauss, lower_perc)
    im_max = np.percentile(image_gauss, upper_perc)
    
    image_gauss = np.clip(image_gauss, im_min, im_max)
    im_norm = (image_gauss.astype(np.float32) - im_min)/(im_max - im_min)
    
    contours = (1.-im_norm).astype(np.float32)
    
    return foreground, contours
    
def compute_foreground_and_contours_all_timepoints(path: str, 
                          threshold_foreground: int,
                          sigma: int,
                          tp_max: Optional[int] = None,
                          transformation_filename: Optional[str] = "transformations.npz",
                          enh_zarrname: Optional[str] = "dataset_enh.ome.zarr",
                          foreground_zarrname: Optional[str] = "dataset_reg_foreground.ome.zarr",
                          contours_zarrname: Optional[str] = "dataset_reg_contours.ome.zarr"
                          ):
    """_summary_

    Args:
        path (str): _description_
        threshold_foreground (int): _description_
        sigma (int): _description_
        tp_max (Optional[int], optional): _description_. Defaults to None.
        transformation_filename (Optional[str], optional): _description_. Defaults to "transformations.npz".
        enh_zarrname (Optional[str], optional): _description_. Defaults to "dataset_enh.ome.zarr".
        foreground_zarrname (Optional[str], optional): _description_. Defaults to "dataset_reg_foreground.ome.zarr".
        contours_zarrname (Optional[str], optional): _description_. Defaults to "dataset_reg_contours.ome.zarr".
    """

    meta = read_meta_csv(path)
    if tp_max is not None:
        meta["n_tp"] = tp_max
        
    foreground_zarr = f"{path}/{foreground_zarrname}"
    contours_zarr = f"{path}/{contours_zarrname}"
    
    print("---> 01")
    
    if ( not os.path.exists(foreground_zarr) ) or ( not os.path.exists(contours_zarr) ):
        
        print("Compute foreground and contours...")
        
        enh_zarr = f"{path}/{enh_zarrname}"
        ds_enh = OmeZarr(enh_zarr, mode="r")

        _, _, ax_scale, ax_shape = ds_enh.get_axes_info( res_level=0 )
        dt = ax_scale[0]
        n_tp = ax_shape[0]

        ds_foreground = OmeZarr(foreground_zarr, mode="a")
        ds_foreground.create_dataset(meta, dt = dt, n_scales = 1)
        
        ds_contours = OmeZarr(contours_zarr, mode="a")
        ds_contours.create_dataset(meta, dt = dt, n_scales = 1, dtype=np.float32)
        
        Ts = np.load( f"{path}/{transformation_filename}" )["Ts"]

        for tp in tqdm.tqdm( range( n_tp ), total=n_tp ):
            
            channel = 0

            image_enhanced = ds_enh.read_stack( time_point=tp, channel=channel )

            foreground, contours = compute_foreground_and_contours ( image_enhanced,
                                                            threshold_foreground = threshold_foreground,
                                                            sigma = sigma,
                                                            )
            
            foreground_reg = perform_image_registration( foreground, tp, Ts )
            contours_reg = perform_image_registration( 1.+contours, tp, Ts, dtype="float" )
            contours_reg[contours_reg==0.] = 2.
            contours_reg -= 1.

            ds_foreground.write_stack(foreground_reg, 
                                 timepoint=tp, 
                                 channel=channel
                                 )    

            ds_contours.write_stack(contours_reg, 
                                 timepoint=tp, 
                                 channel=channel
                                 )    

        ds_enh.close()
        ds_foreground.close()
        ds_contours.close()
        
    else:
        
        print("A foreground and contours dataset already exists.")
    