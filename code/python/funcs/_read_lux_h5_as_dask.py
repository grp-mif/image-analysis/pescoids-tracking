import dask.array as da
import tqdm
import h5py

def read_lux_h5_as_dask(meta, file_name, zyx_chunks=None):

    arrays = [ [ None for ch in range(meta["n_ch"]) ] for tp in range(meta["n_tp"]) ]
    dsets = [ [ h5py.File(file_name%(tp, ch), "r") for ch in range(meta["n_ch"]) ] for tp in range(meta["n_tp"]) ]
    
    zyx_chunks_opt = zyx_chunks
    if zyx_chunks is None:
        zyx_chunks_opt = (int(meta["shape_z"]), int(meta["shape_y"]), int(meta["shape_x"]))

    for tp in range(meta["n_tp"]):

        for ch in range(meta["n_ch"]):

            arrays[tp][ch] = da.from_array(dsets[tp][ch]["Data"], chunks=zyx_chunks_opt)

    arrays = da.stack(arrays, axis=0)

    # print(arrays.shape)
    
    return arrays, dsets, zyx_chunks_opt
