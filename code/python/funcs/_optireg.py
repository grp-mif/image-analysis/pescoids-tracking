
import os, re, glob
import pandas as pd
import numpy as np
import dask.array as da
from tqdm import tqdm
from skimage.transform import resize
from skimage.io import imread, imsave
import pyclesperanto_prototype as cle
from scipy.spatial.transform import Rotation
from scipy import optimize
from _register_icp import cloud_transform, image_transform
from sklearn.neighbors import NearestNeighbors
import subprocess as sp

def get_gpu_memory():
    command = "nvidia-smi --query-gpu=memory.free --format=csv"
    memory_free_info = sp.check_output(command.split()).decode('ascii').split('\n')[:-1][1:]
    memory_free_values = [int(x.split()[0]) for i, x in enumerate(memory_free_info)]
    return memory_free_values

'''
Process images to find most of the cells
'''
def find_anchor_points(img, tp, cell_diameter_XY = 10, threshold = 200):

    # compute gaussian blur
    sigmaxy = cell_diameter_XY
    sigma = [sigmaxy,sigmaxy,sigmaxy]
    
    img_gauss = cle.gaussian_blur(img, sigma_x=sigma[2], sigma_y=sigma[1], sigma_z=sigma[0])

    # find localmaxima
    detected_spots = cle.detect_maxima_box(img_gauss, 
                                        radius_x=0,
                                        radius_y=0,
                                        radius_z=0,)
    selected_spots = cle.binary_and(img_gauss>threshold, detected_spots)
    del img_gauss

    # compute spots coordinates
    p = np.where(selected_spots)

    df = pd.DataFrame({
                        'tp': tp,
                        'z': p[0].astype(float),
                        'y': p[1].astype(float),
                        'x': p[2].astype(float),
                        })

    return df

########################
'''
Find registration via mean distance minimization
'''

def transformation(params):
    angles = params[:3]
    translation = params[3:]
    rot = Rotation.from_rotvec(angles).as_matrix()
    
    # special reflection case
    if np.linalg.det(rot) < 0:
        [U,S,V] = np.linalg.svd(rot)
        # multiply 3rd column of V by -1
        rot = V * U.T
    
    mat = np.eye(4)
    mat[:3,:3] = rot
    mat[:3,3] = translation
    
    return mat

def loss(params, points_ant, points_now):

    points_now_new = cloud_transform(points_now,transformation(params))
    nbrs = NearestNeighbors(n_neighbors=1, algorithm='brute').fit(points_now_new)
    distances, indices = nbrs.kneighbors(points_ant)

    return np.mean(distances)

def find_transformations(df_list, transformation_name = 'transformations.npz', maxiter=100, tol=1e-5):


    if not os.path.exists(transformation_name):

        Ts = [np.eye(4) for i in range(len(df_list))]

        for i in tqdm(range(len(df_list)-1)):

            pos_ant = df_list[i][['z','y','x']].to_numpy()
            pos_now = df_list[i+1][['z','y','x']].to_numpy()

            initial_guess = [0,0,0,0,0,0]

            result = optimize.minimize(loss, initial_guess, 
                                    args = (pos_ant, pos_now), 
                                    options={'maxiter': maxiter}, tol=tol)

            Ts[i+1] = transformation(result.x)

        np.savez(transformation_name, Ts=Ts)

    else:

        Ts = np.load(transformation_name)['Ts']

    return Ts

'''
Apply transformations
'''

def perform_points_registration(df_list, Ts):

    for i in range(len(df_list)):
        df_list[i].loc[:,'xreg'] = np.array(df_list[i]['x'])
        df_list[i].loc[:,'yreg'] = np.array(df_list[i]['y'])
        df_list[i].loc[:,'zreg'] = np.array(df_list[i]['z'])
    
    for i in range(len(df_list)):

        A = df_list[i][['z','y','x']].to_numpy()

        T = np.eye(4)
        for j in range(i,0,-1):
            T = np.dot(Ts[j],T)
        A = cloud_transform(A, T)

        df_list[i].loc[:,'zreg'] = A[:,0]
        df_list[i].loc[:,'yreg'] = A[:,1]
        df_list[i].loc[:,'xreg'] = A[:,2]

    return df_list

def perform_image_registration(image, tp, Ts, dtype="int"):
    
    image_ndarray = image
    ### not necessary, as it seems cle already deals with dask arrays
    if isinstance(image, da.Array):
        image_ndarray = image.compute().astype(dtype)
    # print(type(image_ndarray), image_ndarray.shape)
                
    # find out transformation
    T = np.eye(4)
    for j in range(len(Ts)-tp-1,0,-1):
        T = np.dot(Ts[j],T)

    # compute transformation
    # print('4',get_gpu_memory()[0])
    img_iso = cle.transpose_xz(image_ndarray)
    img_iso = np.asarray(img_iso).astype(dtype)
    # print(type(img_iso), img_iso.shape, np.max(img_iso))
    # print('5',get_gpu_memory()[0])
    img_reg_iso = cle.affine_transform(img_iso, transform=T, 
                                linear_interpolation=True)
    # print('6',get_gpu_memory()[0])
    del img_iso
    # print('7',get_gpu_memory()[0])
    img_reg_iso = cle.transpose_xz(img_reg_iso)
    # print('8',get_gpu_memory()[0])

    if dtype=="int":
        return np.asarray(img_reg_iso).astype(np.uint16)
    else:
        return np.asarray(img_reg_iso).astype(np.float32)
