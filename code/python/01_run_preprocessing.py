import os
import sys
import glob
import argparse
sys.path.append("funcs")
from _preprocess import preprocess

'''
To run this script:

>>> python run_preprocessing.py --share /mif-users --radius_enhanced 1,1,1
'''

def list_of_floats(arg):
    l = [float(f) for f in arg.split(",")]
    # print("---",l)
    return l

parser = argparse.ArgumentParser()
parser.add_argument("--share", 
                    type=str, default="/g/trivedi", 
                    help="Server share. /g/trivedi (Linux) or //trivedi.embl.es/trived (Windows)")
parser.add_argument("--master_folder",
                    type=str, default="Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking")

parser.add_argument("--dt",
                    type=float, default=2.5)

parser.add_argument("--cell_diameter",
                    type=int, default=10)
parser.add_argument("--threshold",
                    type=int, default=50)

parser.add_argument("--radius_enhanced",
                    type=list_of_floats, default=2.,
                    help="Desired radius enhancement in um (provide one value for isotropic ZYX dimension).")

args = parser.parse_args()

share = args.share
master_folder = args.master_folder

dt = args.dt

cell_diameter = args.cell_diameter
threshold = args.threshold

radius_enhanced = args.radius_enhanced

tp_max = 5

#--------------------------------------------------------

paths = glob.glob(os.path.join(share, master_folder, "raw_data", "*"))
paths.sort()
# paths = paths[-2:]
# paths = [path for path in paths if path.endswith("_BAX_C")]
print(paths)

for path in paths:
    print(10*"-",path)
    preprocess(path, share, master_folder, 
               dt, cell_diameter, threshold, radius_enhanced, 
            #    tp_max=tp_max, 
            #    raw_zarrname="dataset_%dtps.ome.zarr"%tp_max,
            #    reg_zarrname="dataset_reg_%dtps.ome.zarr"%tp_max, 
            #    enh_zarrname = "dataset_enh_%dtps.ome.zarr"%tp_max, 
            #    mip_filename="ch-%s_mip_%dtps.tif"%("%d",tp_max)
               )
