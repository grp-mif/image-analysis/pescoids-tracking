import sys
import napari
import pandas as pd
import numpy as np

# sys.path.append("funcs")
# from _ome_zarr import OmeZarr

#######################################################################################

master_folder = "/g/trivedi/Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking"
processed_path = f"{master_folder}/processed_data/pescoid_2024-06-13"
results_path = f"{master_folder}/results/pescoid_2024-06-13"
tp_max = None

#######################################################################################

tp_max_suffix = ""
if tp_max is not None:
    tp_max_suffix = f"_{tp_max}tps"

v = napari.Viewer()

v.open(f"{processed_path}/dataset_reg{tp_max_suffix}.ome.zarr", 
       name="dataset",
       plugin="napari-ome-zarr")

# v.open(f"{processed_path}/dataset_reg_foreground{tp_max_suffix}.ome.zarr",
#        name=f"foreground{tp_max_suffix}",
#        plugin="napari-ome-zarr",
#        contrast_limits=(0, 1),
#     #    layer_type="labels",
#     #    colormap="gist",
#        )

# v.open(f"{processed_path}/dataset_reg_contours{tp_max_suffix}.ome.zarr",
#        name=f"contours{tp_max_suffix}",
#        plugin="napari-ome-zarr",
#        contrast_limits=(0, 1),
#        )

v.open(f"{results_path}/results_ultrack{tp_max_suffix}/my_segments.ome.zarr",
       name=f"labels{tp_max_suffix}",
       plugin="napari-ome-zarr",
    #    contrast_limits=(0, 1),
    #    layer_type="labels",
       )

tracks_df = pd.read_csv(f"{results_path}/results_ultrack{tp_max_suffix}/tracks_features.csv")
graph = np.load(f"{results_path}/results_ultrack{tp_max_suffix}/graph_dict.npy", allow_pickle='TRUE').item()

tracks_df.sort_values(by=["track_id", "t"], inplace=True)
tracks = tracks_df[["track_id", "t", "z", "y", "x"]].to_numpy()

v.add_tracks(
    tracks,
    name=f"tracks{tp_max_suffix}",
    graph=graph,
    features = tracks_df,
    visible=True,
)

napari.run()
