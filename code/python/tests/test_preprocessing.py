import glob, os, tqdm, time, sys
import numpy as np
import pandas as pd
sys.path.append("../funcs")
from _extract_meta import extract_meta
from _open_and_downsample_h5 import open_h5
from _optireg import find_anchor_points, find_transformations, perform_image_registration
from OpenIJTIFF import save_ij_tiff
from _ome_zarr import OmeZarr
# from dexp.datasets import ZDataset

def preprocess(path, share, master_folder,
               cell_diameter=10, threshold=50, tp_max=-1, dt=2.5):
    
    #################################################################################
    # extract metadata and save as csv file
    #################################################################################
    
    meta = extract_meta(path)
    if tp_max>0:
        meta["n_tp"] = tp_max
    print(meta)

    output_folder = os.path.join(share, master_folder, "processed_data", "test_pescoid_%s"%(meta["date"]))
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    if not os.path.exists(os.path.join(output_folder,"mips")):
        os.makedirs(os.path.join(output_folder,"mips"))
    meta = pd.Series(meta)
    meta.to_csv(os.path.join(output_folder, "meta.csv"))

    #################################################################################
    # Compute image transformations to register timepoints
    #################################################################################

    transformation_filename = "transformations.npz"
    print(os.path.exists( os.path.join(output_folder, transformation_filename) ))
    
    if not os.path.exists( os.path.join(output_folder, transformation_filename) ):
        
        print("Compute transformation...")
    
        ### Read images and identify majority of cells
        
        print("Finding anchor points.")

        df = pd.DataFrame({})
        
        for tp in tqdm.tqdm(range(meta["n_tp"]), total=meta["n_tp"]):
            
            ch = 0
            
            image = open_h5(path, ch=ch, tp=tp)
        
            df1 = find_anchor_points(image, tp, cell_diameter_XY = cell_diameter, threshold = threshold)
        
            df = pd.concat([df,df1], ignore_index=True)

        # reverse_order
        df.loc[:,'tpreg'] = max(df.tp)-df.tp
        
        ### Compute transformation and save them
        
        print("Compute and save transformations.")
        
        tps = list(set(df.tpreg))
        tps.sort()
        
        df_list = [df[df.tpreg==tp] for tp in tps]

        Ts = find_transformations(df_list, output_folder, fileName = 'transformations.npz', maxiter=100, tol=1e-5)

        ### df_list = perform_points_registration(df_list, Ts)
        
    else:
        
        ### If it was already computed, load transformation matrices
        
        print("Found transformation. Loading...")
        Ts = np.load( os.path.join(output_folder, transformation_filename) )["Ts"]
        
    #################################################################################
    # Apply transformation to images and save
    #################################################################################

    zarr_filename = os.path.join(output_folder, "dataset_test_omezarr_reg.zarr")
    
    if not os.path.exists(zarr_filename):

        print("Register images.")
    
        # shape = ( int(meta["shape_z"]), int(meta["shape_y"]), int(meta["shape_x"]) )
        ds_reg = OmeZarr(zarr_filename)
        ds_reg.create_dataset(meta, dt = 2.5, n_scales = 2, channel_colors=["00FFFF"])

        for ch in range(meta["n_ch"]):
        
            # ch_name = meta["ch-%d_name"%ch]
            
            for tp in tqdm.tqdm(range(meta["n_tp"]), total=meta["n_tp"]):
            
                image = open_h5(path, ch=ch, tp=tp)
                
                image_reg = perform_image_registration(image, tp, Ts)
                
                ds_reg.write_stack(image_reg, tp, ch)

        ds_reg.close()
        
    else:
        
        print("A registered dataset already exists.")
                
    #################################################################################
    # Compute MIPs and save
    #################################################################################
    
    print("Compute MIP.")
    
    in_ds = OmeZarr(zarr_filename)
    print(in_ds, "opened!!!")
        
    for ch in range(meta["n_ch"]):
        
        ch_name = meta["ch-%d_name"%ch]
        print("Processing channel %d: %s..."%(ch, ch_name))
        
        mip = [[] for i in range(meta["n_tp"])]
        
        for tp in tqdm.tqdm(range(meta["n_tp"]), total=meta["n_tp"]):
    
            stack = in_ds.read_stack(channel=ch, timepoint=tp, res_level=0)
        
            image_mip = np.max(stack, axis=0)
        
            mip[tp] = image_mip

        _ = save_ij_tiff(
                    os.path.join(output_folder, "mips", "ch-%d_mip.tif"%(ch)),
                    np.array(mip),
                    ax_names = "TYX",
                    ax_scales = [1. for i in "TYX"],
                    ax_units = [1, "um", "um"],
            )

    in_ds.close()

if __name__=="__main__":
    share = "/g/trivedi"
    master_folder = "Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking"
    path = share+"/"+master_folder+"/raw_data/2024-06-13_154643"
    cell_diameter = 10
    threshold = 50
    preprocess(path, share, master_folder, 
               cell_diameter, threshold, tp_max=3)
