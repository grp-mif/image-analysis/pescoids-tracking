# %%
import os, shutil, tqdm, time, sys
import numpy as np
from skimage.io import imread
import dask.array as da
import zarr
from ome_zarr import scale, writer, reader, io
sys.path.append("../funcs")
from _open_and_downsample_h5 import open_h5

inpath = "/g/trivedi/Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking/raw_data/2024-06-13_154643/processed/20240621-071421_Task_2_Isot_C"
outpath = "/g/trivedi/Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking/processed_data/pescoid_2024-06-13"

%% [markdown]
# Import data as dask array

# %%
times = np.arange(11).astype(int)#[0,1]#,3,4,5]
channels = [0,1]

# raw_name = os.path.join("..","data_jia_substack","tp%03d_ch%d.tif")
dataset = "data_jia"
raw_name = os.path.join("..",dataset,"tp%03d_ch%d.tif")
movie_zarr = [ 
    [
        imread(os.path.join(raw_name%(t,c)), aszarr=True) 
        for c in channels ] 
    for t in times ]

movie_arrays = [
    [ 
        da.from_zarr(c, chunks=(64,128,128)) 
        for c in m ] 
    for m in movie_zarr ]

raw_stack = da.stack(movie_arrays, axis=0)
print("Dataset dimensions (TCZYX):", raw_stack.shape)

raw_stack

# # %% [markdown]
# # # Resave as ome-zarr

# # %%
# # First, make sure files are opened
# print("Open files...")
# raw_stack[:,:,0,0,0].compute()
# print("\tDone.")

# # %%
# # Clear previously created dataset
# print("Create output folder...")
# fpath = os.path.join("..",dataset+"_write_image.ome.zarr") ### Where you want to write 
# if os.path.exists(fpath):
#     shutil.rmtree(fpath)
# print("\tDone.")

# # %%
# # Create dataset folder
# loc = io.parse_url(fpath, mode="w")
# gr = zarr.group(loc.store)

# # %%
# # %%
# # Startup metadata
# axes_names = tuple("tczyx")
# axes_types = {"t": "time", "c": "channel", "z": "space", "y": "space", "x": "space"}
# axes_units = {"t": "second", "c": "", "z": "micrometer", "y": "micrometer", "x": "micrometer"}
# resolution = {"t": 1, "c": 1, "z": 3, "y": 0.3, "x": 0.3} # in physical_size/pixel
# shape = {ax_name: s for ax_name, s in zip(axes_names, raw_stack.shape)}

# # Desired chunk size
# chunksize = (1, 1, 16, 64, 64)

# # %%
# # Setup downscaling option for the Scaler
# # NOTE that z is not scaled in this case!
# is_scaled = {"t":False, "c":False, "z":False, "y":True, "x":True}
# num_resolutions = 3
# downscale = 2

# # %%
# print("Construct dicts attributes for .zattrs...")
# # Construct axes dict
# axes = [
#     dict(name = ax_name,
#          type = axes_types[ax_name],
#          unit = axes_units[ax_name],
#     ) for ax_name in axes_names
# ]

# # Construct scale dict
# transformations = [
#     [
#         dict(
#             type = "scale",
#             scale = [resolution[ax_name]*(downscale**i) if is_scaled[ax_name] else resolution[ax_name] for ax_name in axes_names],
#             )
#     ] 
#     for i in range(num_resolutions)
# ]
# print("\tDone.")

# # %%
# print("Write ome.zarr dataset...")
# start = time.time()
# writer.write_image(image = raw_stack,
#                    group = gr,
#                    axes = axes,
#                    scaler = scale.Scaler(method = "nearest", 
#                                           downscale = 2, 
#                                           max_layer = num_resolutions - 1
#                                          ), ### Note that now the downscaling method is nearest
#                    coordinate_transformations = transformations,
# #                    compute = False,
#                    storage_options = {"dimension_separator": "/",
#                                       "chunks": chunksize,
#                                      },
#                    name = dataset,
#                    metadata = {"microscope": "Luxendo MuVi SPIM",
#                                "description": "Zebrafish timelapse",
#                               },

#                   )
# end = time.time()
# print("\tDone in %d minutes, %.3f seconds"%((end-start)//60,(end-start)%60))

# # %% [markdown]
# # # # Read data as ome-zarr

# # # %%
# # fpath = os.path.join("..","data_jia.ome.zarr") ### Where you want to write 


# # # %%
# # loc = io.parse_url(fpath, mode="r")
# # zarr_reader = reader.Reader(loc).zarr
# # print(zarr_reader.root_attrs)

# # # %%
# # res = zarr_reader.load("0")
# # print(res.shape)

# # # %%
# # sub = res[0,0,:,:,:]
# # print(np.max(sub.compute()))

# # # %% [markdown]
# # # # Visualize in napari

# # # %%
# # # !napari "../data_jia.ome.zarr"

# # # %%
# # import napari
# # v = napari.Viewer()
# # v.open("../data_jia.ome.zarr", plugin="napari-ome-zarr")

# # # %%
# # all_res = [zarr_reader.load("0"),zarr_reader.load("1"),zarr_reader.load("2")]
# # import napari
# # v = napari.Viewer()
# # v.add_image(all_res, multiscale=True, channel_axis=1, name=["mem","nuc"],
# #            contrast_limits=[[100,3000],[100,2000]])


# # # %%

# # # %%
