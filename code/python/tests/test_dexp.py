import glob, os, sys
import numpy as np
from tqdm import tqdm
from skimage.io import imread
sys.path.append("../funcs")
# from _preprocess import preprocess
# from OpenIJTIFF import open_ij_tiff
from dexp.datasets import ZDataset

path = "/g/trivedi/Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking"
path = path + "/processed_data/pescoid_2024-06-13"
SHAPE = (613,802,805)

flist = glob.glob(path+"/ch-0_tp-*_reg.tif")
flist.sort()
n_tp = 5
flist = flist[:n_tp]
print(flist)

ds = ZDataset(path+"/new_dataset.zarr", mode="w-")

gfp_shape = (n_tp,) + SHAPE  # (25, 512, 1024, 1024)
ds.add_channel(name="GFP", shape=gfp_shape, dtype=np.uint16)

for tp in tqdm(range(ds.nb_timepoints("GFP")), "Converting GFP channel"):
    stack = imread(flist[tp])  # loading the data
    ds.write_stack("GFP", tp, stack)  # saving to our format
    
ds.close()