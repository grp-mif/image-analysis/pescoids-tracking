import os, sys, tqdm
from skimage.io import imread
import dask.array as da

sys.path.append("../funcs")
from _ome_zarr import OmeZarr

inpath = "/g/trivedi/Michael_Glombitza/Projects/Imaging/Olympus/wetting/240605_H2B_celltracking/tracking/raw_data/wetting_pescoid_2024-06-05/"
outpath = "/g/trivedi/Michael_Glombitza/Projects/Imaging/Olympus/wetting/240605_H2B_celltracking/tracking/processed_data/wetting_pescoid_2024-06-05/"

if not os.path.exists(outpath):
    os.makedirs(outpath)
    
infname = "Michael_wetting_celltracking2_H2B_A01_G002.tif"
outfname = "Michael_wetting_celltracking2_H2B_A01_G002.ome.zarr"

a = imread(inpath+infname, aszarr=True)
b = da.from_zarr(a)

n_tp = b.shape[0]
n_ch = b.shape[2]

z = OmeZarr(outpath+outfname, mode="a")
meta = {
    "shape_z": b.shape[1],
    "shape_y": b.shape[3],
    "shape_x": b.shape[4],
    "scale_z": 4,
    "scale_y": 1.3,
    "scale_x": 1.3,
    "n_tp": n_tp,
    "n_ch": n_ch
}
z.create_dataset(meta, dt = 1, n_scales = 3, channel_colors = ["#bfff00","#ffff"], zyx_chunks=(64,256,256))
for tp in tqdm.tqdm(range(n_tp), total = n_tp):
    for ch in range(n_ch):
        z.write_stack(b[tp,:,ch,...], tp, ch)

print(b.shape)
print(b[0,0,...].compute())

print(a)
