import glob, os, sys, math
import numpy as np
from tqdm import tqdm
from skimage.io import imread
import dask.array as da
sys.path.append("../funcs")
from _h5_2_ome_zarr import h5_2_ome_zarr
# from _preprocess import preprocess
# from OpenIJTIFF import open_ij_tiff

path = "/g/trivedi/Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking"
inpath = path + "/raw_data/2024-06-13_154643"
outpath = path + "/processed_data/pescoid_2024-06-13"

h5_2_ome_zarr(inpath, outpath, zarrname="dataset.ome.zarr",
            dt=2.5, n_scales=1, tp_max=-1)

