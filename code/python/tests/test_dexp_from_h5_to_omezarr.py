import glob, os, sys
import numpy as np
from tqdm import tqdm
from skimage.io import imread
sys.path.append("../funcs")
# from _preprocess import preprocess
# from OpenIJTIFF import open_ij_tiff
from dexp.datasets import ZDataset
from _open_and_downsample_h5 import open_h5

path = "/g/trivedi/Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking"
inpath = path + "/raw_data/2024-06-13_154643"
outpath = path + "/processed_data/pescoid_2024-06-13"
SHAPE = (613,802,805)

name = "dataset_dexp_3tps"

############
# try adding the right metadata to have a proper ome-zarr file readable
# I can't, but the ome zarr file is created with to_ome_zarr
############

n_tp = 3

ds = ZDataset(outpath+"/"+name+".zarr", mode="w")

gfp_shape = (n_tp,) + SHAPE  # (25, 512, 1024, 1024)
ds.add_channel(name="nuclear", shape=gfp_shape, dtype=np.uint16, 
               enable_projections=True, chunks=(1,)+SHAPE)

for tp in tqdm(range(ds.nb_timepoints("nuclear")), "Converting nuclear channel"):
    stack = open_h5(inpath, ch=0, tp=tp)  # loading the data
    ds.write_stack("nuclear", tp, stack)  # saving to our format
    
ds.append_metadata(
    {
        "dt": 2.5,
        "dz": 2.3,
        "dy": 1.,
        "dx": 1.,
    }
    # {"multiscales": 
    #     [
    #         {"axes": 
    #             [
    #                 {
    #                     "name": "t",
    #                     "type": "time",
    #                     "unit": "second"
    #                 },
    #                 {
    #                     "name": "z",
    #                     "type": "space",
    #                     "unit": "micrometer"
    #                 },
    #                 {
    #                     "name": "y",
    #                     "type": "space",
    #                     "unit": "micrometer"
    #                 },
    #                 {
    #                     "name": "x",
    #                     "type": "space",
    #                     "unit": "micrometer"
    #                 }
    #             ],
    #         "datasets": 
    #             [
    #                 {
    #                     "coordinateTransformations": [
    #                         {
    #                             "scale": [
    #                                 1,
    #                                 2,
    #                                 1,
    #                                 1
    #                             ],
    #                             "type": "scale"
    #                         }
    #                     ],
    #                     "path": "nuclear"
    #                 },
    #             ]
    #         }
    #     ]
    # }
)

# with n_scales>1 I get an error...
ds.to_ome_zarr(outpath+"/"+name+".ome.zarr", n_scales=1)
    
ds.close()