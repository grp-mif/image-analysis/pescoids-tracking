import glob, os, tqdm, time, sys
import numpy as np
import pandas as pd
sys.path.append("../funcs")
from _extract_meta import extract_meta
from _read_lux_h5_as_dask import read_lux_h5_as_dask
from _ome_zarr import OmeZarr
import dask.array as da

share = "/g/mif"
master_folder = "people/deherdt/P092_2023_exp6_Cx3Cr1_Nerves/M1"
path = share+"/"+master_folder+"/2024-07-17_150916"
tp_max = None
output_folder = path + "/test_ome_zarr_dask"
h5_file = path+"/processed/20240717-140740_Task_1_stitching_C/uni_tp-%d_ch-%d_st-0-x00-y00_obj-bottom_cam-bottom_etc.lux.h5"
chunk_size = (512,1024,1024)
n_scales = 3

#################################################################################
# extract metadata and save as csv file
#################################################################################

meta = extract_meta(path)
if tp_max is not None:
    meta["n_tp"] = tp_max
print(meta)

# output_folder = os.path.join(share, master_folder, "processed_data", "test_pescoid_%s"%(meta["date"]))
if not os.path.exists(output_folder):
    os.makedirs(output_folder)
# if not os.path.exists(os.path.join(output_folder,"mips")):
#     os.makedirs(os.path.join(output_folder,"mips"))
meta = pd.Series(meta)
# meta.to_csv(os.path.join(output_folder, "meta.csv"))

#################################################################################
# Resave image to zarr
#################################################################################

zarr_name = os.path.join(output_folder, "dataset_test_omezarr_dask.ome.zarr")

print("Convert images.")

shape = ( int(meta["shape_z"]), int(meta["shape_y"]), int(meta["shape_x"]) )

image, dsets, chuncks_opt = read_lux_h5_as_dask(meta, h5_file, zyx_chunks=chunk_size)
print(type(image), image.shape)

ds_reg = OmeZarr(zarr_name)
ds_reg.create_dataset(meta, dt = 1., n_scales = n_scales, 
                      channel_colors=["FFFFFF","00FFFF","00FF00","FF00FF"],
                      zyx_chunks=chunk_size)

for ch in tqdm.tqdm(range(meta["n_ch"]), total=meta["n_ch"]):

    for tp in range(meta["n_tp"]):
        
        ds_reg.write_stack_chunks(image[tp, ch], tp, ch )

ds_reg.close()
    
            
# #################################################################################
# # Compute MIPs and save
# #################################################################################

# print("Compute MIP.")

# in_ds = OmeZarr(zarr_filename)
# print(in_ds, "opened!!!")
    
# for ch in range(meta["n_ch"]):
    
#     ch_name = meta["ch-%d_name"%ch]
#     print("Processing channel %d: %s..."%(ch, ch_name))
    
#     mip = [[] for i in range(meta["n_tp"])]
    
#     for tp in tqdm.tqdm(range(meta["n_tp"]), total=meta["n_tp"]):

#         stack = in_ds.read_stack(channel=ch, timepoint=tp, res_level=0)
    
#         image_mip = np.max(stack, axis=0)
    
#         mip[tp] = image_mip

#     _ = save_ij_tiff(
#                 os.path.join(output_folder, "mips", "ch-%d_mip.tif"%(ch)),
#                 np.array(mip),
#                 ax_names = "TYX",
#                 ax_scales = [1. for i in "TYX"],
#                 ax_units = [1, "um", "um"],
#         )

# in_ds.close()

