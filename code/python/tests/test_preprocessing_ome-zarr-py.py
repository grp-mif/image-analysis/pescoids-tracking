# %%
import os, time, sys, glob
import pandas as pd
sys.path.append("../funcs")
from _extract_meta import extract_meta
from _ome_zarr import construct_metadata, write_ome_zarr_to_disk, close_files
from _read_lux_h5_as_dask import read_lux_h5_as_dask

share = "/g/trivedi"
master_folder = "Nick_Marschlich/EMBL_Barcelona/Projects/Imaging/Luxendo/pescoids_tracking"
path = share+"/"+master_folder+"/raw_data/2024-06-13_154643"
cell_diameter = 10
threshold = 50
n_scales = 2
tp_max = 2
dt = 2.5
zyx_chunks = (1024,2048,2048)
channel_colors = ["00FFFF"]
output_folder = os.path.join(share, master_folder, "processed_data", "test_pescoid_%s"%(meta["date"]))
fpath = os.path.join(output_folder,"dataset_ome-zarr-py.ome.zarr") ### Where you want to write 
    
# %% [markdown]
# Import data as dask array

# %%

meta = extract_meta(path)
if tp_max>0:
    meta["n_tp"] = tp_max
print(meta)

if not os.path.exists(output_folder):
    os.makedirs(output_folder)
# if not os.path.exists(os.path.join(output_folder,"mips")):
#     os.makedirs(os.path.join(output_folder,"mips"))
meta = pd.Series(meta)

########################################################

print("Convert images.")
import glob
# shape = ( int(meta["shape_z"]), int(meta["shape_y"]), int(meta["shape_x"]) )
files_path = glob.glob(os.path.join(path, "processed", "*_C"))[0]
file_name = glob.glob(os.path.join(files_path, "*.lux.h5"))[0]
file_name = file_name.replace("tp-0", "tp-%d")
file_name = file_name.replace("ch-0", "ch-%d")
# print(file_name)

arrays, dsets, zyx_chunks = read_lux_h5_as_dask(meta, file_name, zyx_chunks = zyx_chunks)
    

print("Write ome.zarr dataset...")
start = time.time()

axes, transformations = construct_metadata(dt, meta, n_scales)
write_ome_zarr_to_disk(fpath, arrays, meta, axes, n_scales, transformations, zyx_chunks, channel_colors)
close_files(dsets)

end = time.time()
print("\tDone in %d minutes, %.3f seconds"%((end-start)//60,(end-start)%60))


# # %% [markdown]
# # # # Read data as ome-zarr

# # # %%
# # fpath = os.path.join("..","data_jia.ome.zarr") ### Where you want to write 


# # # %%
# # loc = io.parse_url(fpath, mode="r")
# # zarr_reader = reader.Reader(loc).zarr
# # print(zarr_reader.root_attrs)

# # # %%
# # res = zarr_reader.load("0")
# # print(res.shape)

# # # %%
# # sub = res[0,0,:,:,:]
# # print(np.max(sub.compute()))

# # # %% [markdown]
# # # # Visualize in napari

# # # %%
# # # !napari "../data_jia.ome.zarr"

# # # %%
# # import napari
# # v = napari.Viewer()
# # v.open("../data_jia.ome.zarr", plugin="napari-ome-zarr")

# # # %%
# # all_res = [zarr_reader.load("0"),zarr_reader.load("1"),zarr_reader.load("2")]
# # import napari
# # v = napari.Viewer()
# # v.add_image(all_res, multiscale=True, channel_axis=1, name=["mem","nuc"],
# #            contrast_limits=[[100,3000],[100,2000]])


# # # %%

# # # %%
