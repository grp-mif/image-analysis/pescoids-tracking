# Project summary

This project is about tracking cells in pescoids imaged on the MuVi SPIM light sheet system.

# Desired measurements

Running the analysis pipline, we aim to determine cell movements and elongation properties of pescoids.

# Data management

- [Data management](data_management.md)

# Workflow

- [Analysis workflow](analysis_workflow.md)

### Usage instructions

- Download this repository and store on your lab server.
- Access the EMBL Barcelona Linux virtual machine with **remote desktop** connection to `ba-training.embl.es`.
- Open a terminal and navigate to the location of the script you want to run (e.g. `cd /g/trivedi/.../pescoids-tracking/code/python`).
- Activate python environment: `conda activate cle`.
- Open the script you want to run and modify parameters according to your needs (e.g. `run_all.py` and modify `_parse_args.py` and `tp_max`).
- Run the script from terminal: `python run_all.py`. (You can also add optional parameters like so: `python run_all.py --dt 1.`.)

# For developers

**NOTE:** To include screenshots to the readme, please put them in the "documentation" folder.

To make videos to be added to Gitlab, see [instructions](https://git.embl.de/grp-cba/cba/-/blob/master/misc/make-video-from-tif-fiji.md).

### Installation instruction:

This software depends on two main libraries:
- [pyclesperanto-prototype](https://github.com/clEsperanto/pyclesperanto_prototype): for GPU-accelerated image processing.
- [ultrack](https://github.com/royerlab/ultrack): for cell tracking.

To install the packages, please follow the instructions in the pages.

NOTE: the two packages should be installed **in the same python environment**.

On the Linux ba-training virtual machine, these are the installations steps I followed.
- `conda create -n cle python=3.9`
- `conda activate cle`
- `pip install pyclesperanto-prototype`
- `pip install tqdm pandas h5py dask  scikit-learn requests ome_zarr toml rich siphash24 notebook`
- `pip install ultrack`
- `conda install -c gurobi gurobi`
- `grbgetkey YOUR_LICENSE_KEY`
- `conda install ocl-icd-system`
- `pip install -U napari[all]`
- `pip instal napari-ome-zarr`

NOTE: YOUR_LICENSE_KEY key: check on portal.gurobi.com

### Setup of new users

On the ba-training VM, new users need to initiate conda in their session.
They can do so by opening a terminal and running: `/opt/miniconda3/bin/conda init`

After this step, the list of conda environments is 
- avaialble (`conda env list`) and 
- usable (`conda activate <ENV_NAME>`)
- but not modifiable (`pip install numpy` should fail)

### How to update repository:

- make changes
- in terminal inside the repository folder type `git add --all`
- commit: type `git commit -m "a message"`
- type: `git push origin main`

How to take changes from repository to local computer:
- in terminal inside repository folder, type `git pull`
