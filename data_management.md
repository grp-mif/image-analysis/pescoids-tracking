# Data managment

Description of the management of both the microscopy **input** and analysis **output** data.

- [General data management considerations](https://git.embl.de/grp-cba/cba#data-management)
- [Characters to use and avoid in filenames](https://youtu.be/dLziBlI2qlo)

## Data location

The data are in the below folder which are shared (eventually via the DMA) with all project members.

`//trivedi.embl.es/Nick_Marschlich/EMBL_Barcelona/Projects.Imaging/Luxendo/pescoids_tracking`

This contains the following subfolders:

- `raw_data`: primary data acquired by the microscope, and processed with Luxendo image processor to fuse the two opposing camera views. The folder for each pescoid image is labeled as follow: `pescoid_YYYY-MM-dd`.
- `processed_data`: contains the data in `ome.zarr` format and the processed images (blob-enhanced, registered, segmented and contours), as well as the maximum intensity projections and some metadata information.
- `results`: contains all tracking results, including the segmented/tracked images and the tracks csv file.


